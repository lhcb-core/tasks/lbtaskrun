#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Enable bash strict mode
set -euo pipefail
IFS=$'\n\t'

# Define the directory containing the log files, relative to home and hostname
LOG_DIR="$HOME/logs-v3/$(hostname)"

# Get the file that current.log points to
CURRENT_LOG=$(readlink -f "$LOG_DIR/current.log")

echo "Current log file is: $CURRENT_LOG"

# Iterate over all log files in the directory
for FILE in "$LOG_DIR"/*.log; do
  # Check if the file is not the current log file
  if [ "$(readlink -f "$FILE")" != "$CURRENT_LOG" ]; then
    # Compress the file and remove the original
    echo "Compressing $FILE"
    if zstd --rm "$FILE"; then
      echo "Successfully compressed and removed $FILE"
    else
      echo "Failed to compress $FILE"
    fi
  else
    echo "Skipping current log file: $FILE"
  fi
done

echo "Compression of log files completed, excluding the current log file."
