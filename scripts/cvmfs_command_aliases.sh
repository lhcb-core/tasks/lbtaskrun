#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
alias _use_lbcvmfs_message='bash -c "echo Bare cvmfs_ commands should not be used, use the lbcvmfs_ equivalents; exit 1"'
alias cvmfs_abort=_use_lbcvmfs_message
alias cvmfs_config=_use_lbcvmfs_message
alias cvmfs_fsck=_use_lbcvmfs_message
alias cvmfs_publish=_use_lbcvmfs_message
alias cvmfs_receiver=_use_lbcvmfs_message
alias cvmfs_rsync=_use_lbcvmfs_message
alias cvmfs_server=_use_lbcvmfs_message
alias cvmfs_status=_use_lbcvmfs_message
alias cvmfs_suid_helper=_use_lbcvmfs_message
alias cvmfs_swissknife=_use_lbcvmfs_message
alias cvmfs_swissknife_debug=_use_lbcvmfs_message
alias cvmfs_talk=_use_lbcvmfs_message
alias cvmfs_transaction=_use_lbcvmfs_message
