#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -u
set -o pipefail
LOGFILE=${HOME}/logs/install.log
{
    # Parse arguments
    if [ $# -gt 1 ]; then
        echo "Too many arguments supplied" 1>&2
        exit 1
    fi
    repo="$LBTASKRUN_REPO"
    if [ $# -eq 1 ]; then
        repo="${repo}/${1}"
        if [ ! -d "/cvmfs/${repo}" ]; then
            echo "ERROR: Tried to lock a non-existent path: /cvmfs/${repo}" 1>&2
            exit 2
        fi
    fi

    # Lock the repository
    if [ -f "/tmp/disable_transactions" ]; then
        echo -n "ERROR: Transactions are already blocked with message:" 1>&2
        cat "/tmp/disable_transactions" 1>&2
        exit 3
    fi

    echo "Manual transaction started by $(get_original_user) at $(date -u)" > "/tmp/disable_transactions"

    remove_transaction_lock() {
        echo "Caught Control-C, removing lock file" 1>&2
        rm "/tmp/disable_transactions"
    }
    trap remove_transaction_lock EXIT

    cmd_output=$(cvmfs_server transaction "${repo}" 2>/dev/null)
    cmd_code=$?
    if [[ $cmd_code -ne 0 ]] || { [[ "${cmd_output}" != "" ]] && [[ "${cmd_output}" != *"Gateway reply: ok"* ]]; }; then
        echo "Waiting for existing transaction to complete, this might take a while..."
        while [[ $cmd_code -ne 0 ]] || { [[ "${cmd_output}" != "" ]] && [[ "${cmd_output}" != *"Gateway reply: ok"* ]]; }; do
            sleep 10
            echo -n "."
            cmd_output=$(cvmfs_server transaction "${repo}" 2>/dev/null)
            cmd_code=$?
        done
        echo
    fi

    transaction_output=$(cvmfs_server transaction "${repo}" 2>&1)
    if [[ "${transaction_output}" == *"is already in a transaction"* ]] || [[ "${transaction_output}" == *"another transaction is already open"* ]]; then
        echo "Locked repository successfully" 1>&2
    else
        echo "Failed to lock transaction, command output was:" 1>&2
        echo "${transaction_output}" 1>&2
        exit 4
    fi

    # Clear the trap used for capturing Control+C events
    trap - EXIT
} 2>&1 | tee -a "${LOGFILE}"
