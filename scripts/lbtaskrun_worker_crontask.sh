#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
source "${HOME}/.bashrc"

# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

export CELERY_PID_FILE="${SW_LOCATION}/celery_worker.pid"

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
LOG_DIR=$HOME/logs-v3/$HOSTNAME
mkdir -p "${LOG_DIR}"
LATEST_LOG_PATH=${LOG_DIR}/current.log

function start_celery() {
    # Conda's activation scripts can't be used with set -u
    set +u
    source "${SW_LOCATION}/activate_latest" lbtaskrun-env
    set -u
    LOG_FN=$LOG_DIR/$(date -u '+%Y.%m.%d-%H:%M').log
    ln -sf "${LOG_FN}" "${LATEST_LOG_PATH}"
    celery -A "lbtaskrun.cvmfs.instances.${LBTASKRUN_QUEUE}" worker \
        --concurrency=1 --loglevel=info -Q "${LBTASKRUN_QUEUE}" \
        --pidfile="${CELERY_PID_FILE}" -n "${LBTASKRUN_QUEUE}.$(hostname)" > "$LOG_FN" 2>&1
}

if [ -f  "${CELERY_PID_FILE}" ]; then
  kill -0 "$(cat "${CELERY_PID_FILE}")" || start_celery
  file_size=$(stat --dereference -c%s "${LATEST_LOG_PATH}")
  if (( file_size > 1000000000 )); then
    celery -A "lbtaskrun.all" control --destination "celery@$LBTASKRUN_QUEUE.$HOSTNAME" --timeout 5 shutdown
  fi
else
 start_celery
fi
