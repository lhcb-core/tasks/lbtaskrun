#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -e
root="/cvmfs/${LBTASKRUN_REPO}/lib/var/lib/LbEnv"
# check for diffs
if [ -z "$(for d in "$root"/{testing,stable}/* ; do readlink -v "$d" ; done | sort | uniq -u)" ] ; then
  echo "testing and stable look identical: nothing to do"
  exit 0
fi
lbcvmfs_transaction
bk=.bk-$(date -Is)
echo "back up old LbEnv/stable to LbEnv/stable$bk"
mv -v "/cvmfs/${LBTASKRUN_REPO}/lib/var/lib/LbEnv/stable"{,"$bk"}
echo "updating LbEnv/stable from LbEnv/testing"
cp -av "/cvmfs/${LBTASKRUN_REPO}/lib/var/lib/LbEnv/"{testing,stable}
lbcvmfs_publish
