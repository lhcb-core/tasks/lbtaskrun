#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

LBTASKRUN_REF=${LBTASKRUN_REF:-master}
CONDA_ENV_NAME=lbtaskrun-env
MINICONDA_INSTALLER="${SW_LOCATION}/Miniforge3-Linux-x86_64.sh"
CONDA_INSTALL_PREFIX="${SW_LOCATION}/installations/$(date +%s%N)"

if [ ! -f "${MINICONDA_INSTALLER}" ]; then
    curl -L "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh" > "${MINICONDA_INSTALLER}"
fi

echo "Creating new installation of ${LBTASKRUN_REF} in ${CONDA_INSTALL_PREFIX}"
bash "${MINICONDA_INSTALLER}" -b -p "${CONDA_INSTALL_PREFIX}"

# Conda's activation scripts can't be used with set -u
set +u
unset CONDA_SHLVL
eval "$("${CONDA_INSTALL_PREFIX}/bin/python" -m conda shell.bash hook)"
conda config --add channels conda-forge
conda config --set auto_activate_base false

CONDA_SUBDIR=linux-ppc64le mamba create --yes --name sysroot_Linux-ppc64le 'sysroot_linux-ppc64le=2.28'
CONDA_SUBDIR=linux-aarch64 mamba create --yes --name sysroot_Linux-aarch64 'sysroot_linux-aarch64=2.28'

# Ensure git waits rather than doing gc in the background and potenially
# leaving file handles open when the transaction is published
git config --global gc.autoDetach false

curl -L "https://gitlab.cern.ch/lhcb-core/tasks/lbtaskrun/raw/${LBTASKRUN_REF}/environment.yml" > "${CONDA_INSTALL_PREFIX}/environment.yml"
mamba env create --name "${CONDA_ENV_NAME}" --file "${CONDA_INSTALL_PREFIX}/environment.yml"
conda activate "${CONDA_ENV_NAME}"
pip install "git+https://gitlab.cern.ch/lhcb-core/tasks/lbtaskrun.git@${LBTASKRUN_REF}"

# Ensure the apptainer session dir is big enough to be useful
sed -i -E 's@sessiondir max size = [0-9]+@sessiondir max size = 10240@' $CONDA_PREFIX/etc/apptainer/apptainer.conf

# Updating the latest activation symlink
ln -sf "${CONDA_INSTALL_PREFIX}/bin/activate" "${SW_LOCATION}/activate_latest"

echo "Cleaning old installation directories: $(ls --reverse "$SW_LOCATION/installations" | tail -n +6)" || true
(cd "$SW_LOCATION/installations" && rm -rf $(ls --reverse "$SW_LOCATION/installations" | tail -n +6)) || true

echo "You can now switch to the new installation by running:"
echo 'celery -A "lbtaskrun.all" control --destination "celery@$LBTASKRUN_QUEUE.$HOSTNAME" --timeout 5 shutdown'
