###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Meta module for accessing a celery application with all tasks present

Used when lauching flower.
"""

from __future__ import annotations

__all__ = [
    "app",
    "beat",
    "gateway_lhcbtest",
    "lhcbdev",
    "lhcbprod",
    "lhcbtest",
]

from . import app, beat
from .cvmfs.instances import gateway_lhcbtest, lhcbdev, lhcbprod, lhcbtest
