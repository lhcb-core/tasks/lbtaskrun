###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from celery.schedules import crontab

from lbtaskrun import app
from lbtaskrun.cvmfs.instances import gateway_lhcbtest, lhcbcondb, lhcbprod, lhcbtest
from lbtaskrun.management import alert_lbtaskrun_status


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Management
    app.add_periodic_task(
        crontab(minute="*/5"), alert_lbtaskrun_status.s(), expires=5 * 60
    )

    # lhcbdev (referred to as lhcbtest due to historical reasons)
    app.add_periodic_task(
        crontab(hour=21, minute=0), lhcbtest.cleanup_nightlies.s(), expires=4 * 60 * 60
    )
    app.add_periodic_task(
        crontab(hour=22, minute=30), gateway_lhcbtest.gc.s(), expires=4 * 60 * 60
    )
    app.add_periodic_task(
        crontab(hour="*/4", minute=10),
        lhcbtest.rsync_certificates.s(),
        expires=4 * 60 * 60,
    )

    # lhcb-prod
    app.add_periodic_task(
        crontab(hour="*/2", minute=15),
        lhcbprod.rsync_certificates.s(),
        expires=2 * 60 * 60,
    )
    app.add_periodic_task(
        crontab(minute=12, hour=15),
        lhcbprod.update_platforms_list.s(),
        expires=20 * 60 * 60,
    )
    app.add_periodic_task(
        crontab(minute=17, hour=5),
        lhcbprod.create_lbenv.s(),
        expires=20 * 60 * 60,
    )
    app.add_periodic_task(
        crontab(minute=15, hour=8),
        lhcbprod.deploy_lhcbpilot.s(),
        expires=20 * 60 * 60,
    )

    # lhcb-condb
    app.add_periodic_task(
        crontab(minute=0), lhcbcondb.git_update_lcg_toolchain.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=5), lhcbcondb.git_update_dddb.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=10), lhcbcondb.git_update_dqflags.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=15), lhcbcondb.git_update_lhcbcond.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=20), lhcbcondb.git_update_online.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=25), lhcbcondb.git_update_simcond.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=30), lhcbcondb.git_update_stcond.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=35), lhcbcondb.git_update_velocond.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=40), lhcbcondb.git_update_conditions.s(), expires=60 * 60
    )
    app.add_periodic_task(
        crontab(minute=45),
        lhcbcondb.git_update_file_content_metadata.s(),
        expires=60 * 60,
    )
    app.add_periodic_task(
        crontab(minute=50), lhcbcondb.git_update_lhcbstacks.s(), expires=60 * 60
    )
