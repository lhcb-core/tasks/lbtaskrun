###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os
import ssl
from urllib.parse import urlsplit

broker_url = os.environ.get("LBTASKRUN_CELERY_BROKER")
if os.environ.get("LBTASKRUN_CELERY_USE_SSL", True):
    broker_use_ssl = {
        "cert_reqs": ssl.CERT_REQUIRED,
        "server_hostname": urlsplit(broker_url).hostname,
    }
result_backend = os.environ.get("LBTASKRUN_CELERY_BACKEND")

del os
del ssl
del urlsplit
