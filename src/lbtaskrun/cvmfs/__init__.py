###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import json
import logging
import os
import shlex
import shutil
import subprocess
import tempfile
import time
from io import BytesIO
from os.path import isdir
from pathlib import Path
from tarfile import TarFile
from zipfile import ZipFile

import celery
import requests

import lbtaskrun
from lbtaskrun import rpm_utils, utils
from lbtaskrun.cvmfs.transaction import CvmfsLock, transactions_disabled

from . import conda, lbenv, new_nightlies
from .gateway import wait_until_transactions_done
from .git import check_and_update, check_and_update_bare
from .nightlies import add_nightlies_link, get_build_date, get_nightlies_to_gc

logger = logging.getLogger(__name__)


def safe_list_dir(path: Path):
    """List a directory, returning empty if an OSError is raised

    This is helpful to avoid issues where listing CVMFS fails with
    "Endpoint not connected".
    """
    try:
        return list(path.iterdir())
    except OSError:
        return []


def find_dir_to_lock(repo_name, nightlies_path, slot_name, build_id):
    """
    Find the lower level at which we can lock. It must already exist in CVMFS
    """
    # To know which directory to lock, we need to find out where the nightlies
    # are situated in the repository... This *should* be the nightlies_path
    # from which we remove the '/cvmfs', except in test cases....
    subpath = str(nightlies_path).replace("/cvmfs/", "")
    if not subpath.startswith(repo_name):
        logging.warning(
            f"Inconsistent repository {repo_name} configuration. "
            f"Nightlies in {nightlies_path}"
        )

    # CVMFS does not allow a lease on non existing directories.
    # We have to find the most granular lock we can take
    # We try by the build ID path and go up until we find a directory created...
    paths = [str(o) for o in [nightlies_path, slot_name, build_id]]
    transaction_lock_path = None
    while not transaction_lock_path and len(paths) > 0:
        if os.path.exists(os.path.join(*paths)):
            transaction_lock_path = os.path.join(subpath, *paths[1:])
            logging.info(f"Locking: {transaction_lock_path}")
        else:
            paths = paths[:-1]

    # At this stage if no path was found, i.e. nightlies_path was not even created
    # we ask for it to be done first
    if transaction_lock_path is None:
        msg = f"{nightlies_path} does not exist. Please create it before deployment"
        raise Exception(msg)

    return transaction_lock_path


def install_nightly(
    repo_name,
    nightlies_path,
    slot_name,
    project_name,
    build_id,
    platforms,
    restrict_lease=False,
):
    """Invoke a project from the nightlies using the lbn-install command
    The restrict_lease argument should be used on a multi-manager CVMFS
    stratum-0 setup.
    If Yes, then a lock corresponding to the nightly slot is taken instead of
    the whole repository.
    """

    logging.info(
        "Requested installation of %s %s %s %s",
        slot_name,
        project_name,
        build_id,
        platforms,
    )

    # We shouldn't do this but the logic on lbn-install seems to be in the
    # main function...
    args = [
        "lbn-install",
        f"--dest={nightlies_path}/{slot_name}/{build_id}",
        "--debug",
        "--no-git",
        f"--projects={project_name}",
    ]
    if platforms:
        args.append("--platforms=" + ",".join(platforms))
    args += [slot_name, str(build_id)]

    do_retry = False
    if transactions_disabled():
        logger.info("Transactions are currently disabled on %s", repo_name)
        do_retry = True
    elif not safe_list_dir(Path("/cvmfs") / repo_name.split("/")[0]):
        logger.info("Applying workaround for CVMFS bug with stuck transactions")
        do_retry = True

    if do_retry and celery.current_task:
        # Sleep to avoid needlessly retrying tasks
        time.sleep(10)
        if celery.current_task.priority:
            raise celery.current_task.retry(
                countdown=30, priority=max(celery.current_task.priority, 0)
            )
        else:
            raise celery.current_task.retry(countdown=300)

    transaction_lock_path = repo_name
    if restrict_lease:
        transaction_lock_path = find_dir_to_lock(
            repo_name, nightlies_path, slot_name, build_id
        )

    with lbtaskrun.CvmfsTransaction(transaction_lock_path):
        logging.info("Running: %s", " ".join(args))
        ret = utils.logging_subprocess_run(args)
        if ret.returncode != 0:
            msg = "Error installing nightly slot"
            raise utils.LbTaskRunSubprocessError(msg, ret)
        logging.info("Checking the files installed")
        clean_cvmfs_path(os.path.join(str(nightlies_path), slot_name, str(build_id)))
        logging.info("Creating symlinks")
        build_date = get_build_date(slot_name, build_id)
        add_nightlies_link(f"{nightlies_path}/{slot_name}", str(build_id), build_date)

    return utils.called_process_to_json(ret)


def clean_cvmfs_path(path, remove_catalog_normalfiles=True):
    """Cleanup a path to remove files that cannot be published in a CVMFS transaction"""
    CATALOGFILE = ".cvmfscatalog"
    for root, dirs, files in os.walk(path):
        if CATALOGFILE in files:
            fullcat = os.path.join(root, CATALOGFILE)
            if os.path.islink(fullcat):
                logging.warning(f"Removing illegal .cvmfscatalog as link {fullcat}")
                os.unlink(fullcat)
            elif os.path.isfile(fullcat) and remove_catalog_normalfiles:
                logging.warning(f"Removing .cvmfscatalog file {fullcat}")
                os.unlink(fullcat)
        if CATALOGFILE in dirs:
            fullcat = os.path.join(root, CATALOGFILE)
            dirs[:] = [d for d in dirs if d != CATALOGFILE]
            logging.warning(f"Removing illegal .cvmfscatalog as directory {fullcat}")
            shutil.rmtree(fullcat)


def deploy_lhcbdirac():
    """Invoke the deployLHCbDirac bash script that fetches the approriate git
    repo and installs the required versions of LHCbDirac"""
    logging.info("Deploying LHCbDirac")
    ret = subprocess.run(["deployLHCbDirac"], capture_output=True, check=False)
    if ret.returncode != 0:
        msg = "Error deploying LHCbDirac"
        raise Exception(msg)
    return utils.called_process_to_json(ret)


def cvmfs_garbage_collect(repo_name, gateway_url):
    """Invoke the garbage collection for the repository"""

    logging.info(f"Requested gc of {repo_name}")

    # First we create the lock file to make sure the release managers stop
    # after they last job
    with CvmfsLock("Garbage collect"):
        # We make sue there is no activity any more
        wait_until_transactions_done(gateway_url)

        # Now we now the release managers have finished
        cmd = ["cvmfs_server", "gc", "-t", "3 days ago", "-f", repo_name]
        ret = subprocess.run(cmd, capture_output=True, check=False)
        return utils.called_process_to_json(ret)


def cleanup_nightlies(repo_name, nightlies_path):
    """Remove old nightlies installations from the repository"""

    logging.info(f"Requested cleaning of nightlies from {nightlies_path}")
    toremove = get_nightlies_to_gc(nightlies_path)
    removed = []
    with CvmfsLock("Cleanup nightly builds"):
        with lbtaskrun.CvmfsTransaction(repo_name, ignore_disabled=True):
            for d in toremove:
                if not str(d).startswith(nightlies_path):
                    logging.error(f"Cannot remove dir not in repo {d}")
                    continue
                logging.info(f"Removing {d}")
                shutil.rmtree(d, ignore_errors=True)
                removed.append(d)

    return removed


def download_and_install_zip(url, extract_location):
    """Download the URL specified and extract it"""
    logging.info(f"Extracting {url} to {extract_location}")

    with tempfile.TemporaryDirectory() as tmpdirname:
        logging.debug(f"Created temporary directory {tmpdirname}")
        local_filename = url.split("/")[-1]
        fullpath = os.path.join(tmpdirname, local_filename)
        with requests.get(url, stream=True, timeout=10) as r:
            with open(fullpath, "wb") as f:
                shutil.copyfileobj(r.raw, f)
        logging.info(f"Downloaded URL to {fullpath}")
        with ZipFile(fullpath, "r") as myzip:
            myzip.extractall(extract_location)
        logging.info("Extraction done")


def cvmfs_download_and_install_zip(repo_name, url, extract_location):
    """Download the URL specified and extract it"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        download_and_install_zip(url, extract_location)


def cvmfs_lbinstall(repo_name, siteroot, parent_install_area, rpm_list):
    """Basic installer of packages using lbinstall"""
    logging.info("Installing RPMS")
    with lbtaskrun.CvmfsTransaction(repo_name):
        cmd = [
            "lbinstall",
            f"--root={siteroot}",
            f"--chained_installarea={parent_install_area}",
            "install",
        ]
        cmd += rpm_list
        ret = subprocess.run(cmd, capture_output=True, check=False)
        logging.info("Running " + " ".join(cmd))
        if ret.returncode != 0:
            msg = "Error running lbinstall"
            raise Exception(msg)
        return utils.called_process_to_json(ret)


def rsync_certificates(repo_name, target_location):
    """Synchronize certificates on CVMFS"""
    logging.info("Synchronizing certificates")
    with lbtaskrun.CvmfsTransaction(repo_name):
        if not os.path.isdir(target_location):
            os.makedirs(target_location)
        cmd = ["rsync", "-av", "/etc/grid-security/certificates", target_location]
        ret = subprocess.run(cmd, capture_output=True, check=False)
        logging.info("Running " + " ".join(cmd))
        if ret.returncode != 0:
            msg = "Error running rsync"
            raise utils.LbTaskRunSubprocessError(msg, ret)
        return utils.called_process_to_json(ret)


def create_lbenv(repo_name, install_root, *, flavours=None, platforms=None):
    """Create updated LbEnv environments"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        lbenv.create_environments(install_root, flavours=flavours, platforms=platforms)


def update_platforms_list(repo_name, target_location):
    """Update cache of available platforms"""
    logging.info(f"Updating platform list ${target_location}")
    with lbtaskrun.CvmfsTransaction(repo_name):
        response = requests.get(
            "https://lhcb-couchdb.cern.ch/nightlies-release/"
            "_design/names/_view/platforms?group=true",
            verify=utils.get_cern_certs_location(),
            timeout=10,
        )
        response.raise_for_status()
        platforms = response.json()
        if len(platforms) < 1:
            raise ValueError(platforms)
        logging.info(f"We now have {len(platforms)} platforms in the list")
        with open(target_location, "w") as fp:
            json.dump(platforms, fp)


def update_project_platforms_from_lhcbstacks_latest(repo_name, target_location):
    """Update cache of application versions and platforms"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        response = requests.get(
            "https://gitlab.cern.ch/lhcb-core/lhcbstacks/"
            "-/jobs/artifacts/master/raw/projects_platforms.json"
            "?job=generate_projects_platforms",
            verify=utils.get_cern_certs_location(),
            timeout=10,
        )
        response.raise_for_status()
        project_platforms = response.json()
        if len(project_platforms.keys()) < 1:
            raise ValueError(project_platforms)
        with open(target_location, "w") as fp:
            json.dump(project_platforms, fp)

        # debugging to understand update issue
        # we keep a copy in /tmp
        import datetime

        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open(f"/tmp/project-platforms-{now}.json", "w") as dbg:
            json.dump(project_platforms, dbg)


def update_project_platforms_from_lhcbstacks(repo_name, target_location, pipeline_id):
    """Update cache of application versions and platforms from a specific pipeline"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        logging.info(
            f"Updating ${target_location} with data from pipeline {pipeline_id}"
        )
        project_platforms = utils.get_pipeline_project_platforms(pipeline_id)
        if len(project_platforms.keys()) < 1:
            raise ValueError(project_platforms)
        with open(target_location, "w") as fp:
            json.dump(project_platforms, fp)


def update_project_info_from_lhcbstacks(repo_name, target_location, pipeline_id):
    """Update cache of application information  and platforms from a specific pipeline"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        logging.info(
            f"Updating ${target_location} with data from pipeline {pipeline_id}"
        )
        project_info = utils.get_pipeline_project_info(pipeline_id)
        if len(project_info.keys()) < 1:
            raise ValueError(project_info)
        with open(target_location, "w") as fp:
            json.dump(project_info, fp)


def update_lcg_info_from_lhcbstacks(repo_name, target_location, pipeline_id):
    """Update cache of LCG information from a specific pipeline"""
    with lbtaskrun.CvmfsTransaction(repo_name):
        logging.info(
            f"Updating ${target_location} with data from pipeline {pipeline_id}"
        )
        lcg_info = utils.get_pipeline_lcg_info(pipeline_id)
        if len(lcg_info.keys()) < 1:
            raise ValueError(lcg_info)
        with open(target_location, "w") as fp:
            json.dump(lcg_info, fp)


def git_update(repo_name, target_location, *, bare=False, validator=None):
    """Update a CVMFS repository"""
    logging.info(f"Updating git repo {target_location}")
    with lbtaskrun.CvmfsTransaction(repo_name):
        if bare:
            needed_update = check_and_update_bare(target_location)
        else:
            needed_update = check_and_update(target_location)
        if not needed_update:
            # In that case no need to publish an empty transaction
            raise lbtaskrun.CvmfsAbortTransaction()
        if validator and not validator(target_location):
            msg = f"Failed to validate new repository content in {target_location}"
            raise Exception(msg)


def deploy_conda_environment_v2(env_yaml, target_location, subdir):
    """Install conda environments for LbConda

    https://gitlab.cern.ch/lhcb-core/conda-environments
    """
    logging.info(
        f"Deploying conda environment to {target_location} with {env_yaml} for {subdir}"
    )
    abs_target_location = Path(target_location)
    if target_location.startswith("/cvmfs/"):
        transaction_lock_path = target_location[len("/cvmfs/") :]
    else:
        raise NotImplementedError(target_location)
    if transaction_lock_path.startswith("lhcbdev.cern.ch"):
        # If this is the first time this environment is being deployed we need to make
        # the parent directory
        if not abs_target_location.parent.is_dir():
            conda_base_dir = "/".join(transaction_lock_path.split("/")[:2])
            with lbtaskrun.CvmfsTransaction(conda_base_dir):
                abs_target_location.parent.mkdir(parents=True)
    else:
        transaction_lock_path = transaction_lock_path.split("/")[0]

    with lbtaskrun.CvmfsTransaction(transaction_lock_path):
        conda.deploy_environment_v2(env_yaml, Path(target_location), subdir)


def install_lhcbdirac(
    version, repo_name, target_location, *, diracos_version=None, alt_arch=True
):
    """Install LHCbDIRAC"""
    logging.info(
        f"Installing LHCbDIRAC {version} in {target_location} with DIRACOS={diracos_version}"
    )
    with lbtaskrun.CvmfsTransaction(repo_name):
        if "." not in version:
            msg = "Python 2 versions are no longer supported"
            raise NotImplementedError(msg)
        timestamp = f"{time.time():.0f}"
        _install_lhcbdirac_py3(
            version,
            target_location,
            "Linux",
            "x86_64",
            timestamp,
            diracos_version=diracos_version,
        )
        if not alt_arch:
            return

        with tempfile.TemporaryDirectory() as tmpdir:
            container_dir = Path(tmpdir) / "container"
            _build_container(container_dir)
            _install_lhcbdirac_py3(
                version,
                target_location,
                "Linux",
                "ppc64le",
                timestamp,
                container_dir,
                diracos_version=diracos_version,
            )
            _install_lhcbdirac_py3(
                version,
                target_location,
                "Linux",
                "aarch64",
                timestamp,
                container_dir,
                diracos_version=diracos_version,
            )


def _build_container(container_dir):
    container_dir.parent.mkdir(parents=True, exist_ok=True)
    ret = utils.logging_subprocess_run(
        [
            "apptainer",
            "build",
            "--fix-perms",
            "--sandbox",
            container_dir,
            "docker://almalinux/9-minimal",
        ]
    )
    ret.check_returncode()
    qemu_url = (
        "https://github.com/multiarch/qemu-user-static/releases/download/v6.1.0-8"
    )
    for arch in ["ppc64le", "aarch64"]:
        path = container_dir / "usr" / "bin" / f"qemu-{arch}-static"
        response = requests.get(f"{qemu_url}/{path.name}", timeout=60)
        response.raise_for_status()
        path.write_bytes(response.content)
        path.chmod(0o755)


def _run_in_container(
    container_dir, cmd, *, ro_binds=None, rw_binds=None, qemu_ld_prefix=None
):
    env = {}
    if qemu_ld_prefix:
        env["QEMU_LD_PREFIX"] = qemu_ld_prefix
        ro_binds = (ro_binds or []) + [qemu_ld_prefix]
    base_cmd = ["apptainer", "exec"]
    for bind in ro_binds or []:
        base_cmd += [f"--bind={bind}:{bind}:ro"]
    for bind in rw_binds or []:
        base_cmd += [f"--bind={bind}:{bind}"]
    for key, value in env.items():
        base_cmd += [f"--env={key}={value}"]
    with tempfile.TemporaryDirectory() as tmpdir:
        base_cmd += ["--containall", f"--workdir={tmpdir}", container_dir]
        return utils.logging_subprocess_run(base_cmd + cmd)


def _install_lhcbdirac_py3(
    version,
    target_location,
    os_,
    arch,
    timestamp,
    container_dir=None,
    diracos_version=None,
):
    url = "https://github.com/DIRACGrid/DIRACOS2/"
    if diracos_version is None:
        url += f"releases/latest/download/DIRACOS-{os_}-{arch}.sh"
    else:
        url += f"releases/download/{diracos_version}/DIRACOS-{os_}-{arch}.sh"

    parent_dir = f"{target_location}/versions/{version}-{timestamp}"
    os.makedirs(parent_dir, exist_ok=True)
    dest_dir = f"{target_location}/versions/{version}-{timestamp}/{os_}-{arch}"
    dest_link = f"{target_location}/{version}"

    if container_dir:
        target_sysroot = (
            Path(os.environ["CONDA_PREFIX"]).parent / f"sysroot_{os_}-{arch}"
        )
        if not target_sysroot.is_dir():
            msg = f"sysroot does not exist: {target_sysroot}"
            raise RuntimeError(msg)
        sysroot_name = f"{arch.replace('ppc64le', 'powerpc64le')}-conda-linux-gnu"
        qemu_ld_prefix = str(target_sysroot / sysroot_name / "sysroot")
        container_kwargs = {"rw_binds": [parent_dir], "qemu_ld_prefix": qemu_ld_prefix}

    result = []
    with tempfile.TemporaryDirectory() as tmpdir:
        installer_fn = Path(tmpdir) / f"DIRACOS-{os_}-{arch}.sh"
        # Download DIRACOS2
        r = requests.get(url, timeout=300)
        r.raise_for_status()
        installer_fn.write_bytes(r.content)

        # Install DIRACOS2
        cmd = ["bash", installer_fn, "-p", dest_dir]
        if container_dir:
            ret = _run_in_container(
                container_dir, cmd, ro_binds=[installer_fn], **container_kwargs
            )
        else:
            ret = utils.logging_subprocess_run(cmd)
        if ret.returncode != 0:
            msg = "Error installing DIRACOS2"
            raise utils.LbTaskRunSubprocessError(msg, ret)
        result += [utils.called_process_to_json(ret)]
    # Install the requested version of LHCbDIRAC
    cmd = f"source {shlex.quote(dest_dir)}/diracosrc && "
    # See https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/blob/master/setup.cfg
    cmd += "micromamba install -y uproot fsspec-xrootd pydantic oracledb cwltool pandas && "
    # cx_oracle=8.1 isn't available on conda-forge for all platforms
    # when it is we should install the "server" extra
    cmd += f"pip install LHCbDIRAC[pilot,admin]=={shlex.quote(version)}"
    if container_dir:
        ret = _run_in_container(container_dir, ["bash", "-c", cmd], **container_kwargs)
    else:
        ret = utils.logging_subprocess_run(cmd, shell=True)
    if ret.returncode != 0:
        msg = "Error installing LHCbDIRAC"
        raise utils.LbTaskRunSubprocessError(msg, ret)
    result += [utils.called_process_to_json(ret)]
    # Manage the symlinks
    if os.path.exists(dest_link):
        os.unlink(dest_link)
    os.symlink(parent_dir, dest_link)

    if os_ == "Linux" and arch == "x86_64":
        legacy_dest_dir = f"{target_location}/versions/{version}-{arch}-{timestamp}"
        legacy_dest_link = f"{target_location}/{version}-{arch}"
        os.symlink(dest_dir, legacy_dest_dir)
        if os.path.exists(legacy_dest_link):
            os.unlink(legacy_dest_link)
        os.symlink(dest_dir, legacy_dest_link)

    return result


def lhcbdirac_set_prod_link(version, repo_name, target_location):
    """Update the "prod" symlink that is defined the default LHCbDIRAC version"""
    logging.info(f"Setting LHCbDIRAC prod link to be {version} in {target_location}")
    with lbtaskrun.CvmfsTransaction(repo_name):
        if "." not in version:
            msg = "Python 2 versions are no longer supported"
            raise NotImplementedError(msg)
        _lhcbdirac_set_prod_link_py3(version, target_location)


def _lhcbdirac_set_prod_link_py3(version, target_location):
    install_path = os.path.join(target_location, version)
    # Sanity check to avoid breaking the prod link
    if not os.path.isfile(f"{install_path}-{os.uname().machine}/bin/python"):
        msg = f"No python binary at {install_path}"
        raise FileNotFoundError(msg)

    # TODO Everything before this is a legacy workaround and can soon be removed
    target_location = Path(target_location)
    for name in ["prod", "prod.py3"]:
        prod_file = target_location / name
        if prod_file.exists():
            prod_file.unlink()
        prod_file.symlink_to(version, target_is_directory=True)
        if not (prod_file / "Linux-x86_64" / "bin" / "python").exists():
            msg = f"No python binary for {install_path=} {version=}"
            raise FileNotFoundError(msg)


def deploy_lhcbdirac_pilot(repo_name, target_location):
    """Deploy LHCbDIRAC pilot"""
    logging.info("Deploying LHCbDIRAC pilot")

    target_location = Path(target_location)

    with lbtaskrun.CvmfsTransaction(repo_name):
        # get pilot.tar, pilot.json, LHCbPilotCommands.py and checksums.sha512 from https://lhcbdirac.s3.cern.ch/Pilot3/
        url = "https://lhcbdirac.s3.cern.ch/Pilot3/"
        files = ["pilot.tar", "pilot.json", "LHCbPilotCommands.py", "checksums.sha512"]

        for f in files:
            file_url = url + f
            logging.info(f"Downloading {file_url}")
            response = requests.get(file_url, timeout=60)
            response.raise_for_status()
            if f == "pilot.tar":
                TarFile(fileobj=BytesIO(response.content)).extractall(
                    target_location, filter="data"
                )

            with open(target_location / f, "wb") as fl:
                fl.write(response.content)


def install_datapkg(rpm_fn, url, repo_name, target_location):
    logging.info(f"Installing {rpm_fn} from {url}")

    rpm_project, rpm_pkg_name, rpm_pkg_version, rpm_build_id = rpm_utils.parse_fn(
        rpm_fn
    )

    rpm_utils.add_url_to_repo(rpm_fn, url)

    with lbtaskrun.CvmfsTransaction(repo_name):
        ret = utils.logging_subprocess_run(
            ["lbinstall", "install", f"{rpm_project}_{rpm_pkg_name}_{rpm_pkg_version}"]
        )
        if ret.returncode != 0:
            msg = "Error in lbinstall"
            raise utils.LbTaskRunSubprocessError(msg, ret)

        rpm_pkg_dir = rpm_pkg_name.replace("_", os.sep)
        target_location = (
            f"/cvmfs/{repo_name}/lib/lhcb/{rpm_project}/{rpm_pkg_dir}/{rpm_pkg_version}"
        )
        if not isdir(target_location):
            msg = f"{target_location} was not created by lbinstall"
            raise RuntimeError(msg)
        if not os.listdir(target_location):
            msg = f"{target_location} is empty"
            raise RuntimeError(msg)


def install_release(build_id, repo_name):
    """Query the nightlies DB for the RPMs produced, copy
    them to the RPM repo and proceed     to the installation"""
    logging.info(f"Installing release {build_id} to {repo_name}")

    # Query the Nightlies DB for the RPM list
    rpms_to_install = rpm_utils.list_rpms_in_build(build_id)

    # Install them one by one in the RPM repo
    for rpm_name in rpms_to_install:
        logging.debug(f"Deploying {rpm_name}")
        url = rpm_utils.get_build_artefact_url(build_id, rpm_name)
        rpm_fn = rpm_utils.get_rpm_fullname(rpm_name)
        logging.info(f"Adding to repository: {url}")
        rpm_utils.add_url_to_repo(rpm_fn, url)

    with lbtaskrun.CvmfsTransaction(repo_name):
        for rpm_name in rpms_to_install:
            ret = utils.logging_subprocess_run(["lbinstall", "install", rpm_name])
            if ret.returncode != 0:
                msg = "Error in lbinstall"
                raise utils.LbTaskRunSubprocessError(msg, ret)


def install_nightly_conda_env(base_prefix, env_hash, env_yaml, subdir):
    prefix = Path(base_prefix) / env_hash
    logging.info(f"Installing nightly conda environment in {prefix}")
    with lbtaskrun.CvmfsTransaction(prefix.relative_to("/cvmfs")):
        if prefix.exists():
            os.utime(prefix)
            return "Already exists, just setting utime on directory"
        else:
            new_nightlies.create_conda_env(prefix, env_yaml, subdir)
            return "Created new environment"
