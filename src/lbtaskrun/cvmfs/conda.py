###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

__all__ = ("deploy_environment_v2",)

import tempfile
from pathlib import Path

from lbtaskrun.utils import logging_subprocess_run

from .lbenv import _create_environment


def deploy_environment_v2(env_yaml: str, destination: Path, subdir: str):
    # Create the environment inside a container for security
    with tempfile.TemporaryDirectory() as tmp:
        tmp_dir = Path(tmp)

        env_yaml_fn = tmp_dir / "conda-lock.yml"
        env_yaml_fn.write_text(env_yaml)

        container_dir = Path(tmp_dir) / "container"
        ret = logging_subprocess_run(
            [
                "apptainer",
                "build",
                "--fix-perms",
                "--sandbox",
                container_dir,
                "docker://almalinux/9-minimal",
            ]
        )
        ret.check_returncode()

        _create_environment(
            tmp_dir,
            container_dir,
            env_yaml_fn,
            destination,
            subdir,
        )

    # Create the environment.yaml file
    # Use mamba here as micromamba is buggy
    ret = logging_subprocess_run(
        ["mamba", "env", "export", "--prefix", destination / subdir]
    )
    ret.check_returncode()
    (destination / f"{subdir}.yaml").write_text(ret.stdout.decode("utf-8"))

    # Create the catalog
    (destination / subdir / ".cvmfscatalog").write_text("")
