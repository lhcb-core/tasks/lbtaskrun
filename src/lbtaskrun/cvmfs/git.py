###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module to update Git repositories in CVMFS
"""

from __future__ import annotations

import logging
from itertools import dropwhile, islice, takewhile

import git
from yaml import safe_load


def validate_gitcondb(repo_path):
    error = False
    with git.Repo(repo_path) as repo:
        for tag in repo.tags:
            if not gitcondb_tag_valid(tag.object.message):
                logging.error("Failed to parse tag metadata for %s", tag.name)
                error = True
    return not error


def gitcondb_tag_valid(s):
    """Check if a tag's GitCondDB metadata is valid

    Based on `_parse_metadata` in
    LHCb/-/blob/master/Tools/GitEntityResolver/python/GitCondDB/Tags.py
    """
    try:
        skipone = lambda iterable: islice(iterable, 1, None)  # NOQA
        not_marker = lambda l: l.rstrip() != "---"  # NOQA
        data = "\n".join(
            takewhile(not_marker, skipone(dropwhile(not_marker, s.splitlines())))
        )
        if data:
            safe_load(data)
    except Exception:
        logging.exception("Failed to parse tag metadata: %r", s)
        return False
    else:
        return True


def local_clone_needs_update(local_clone, origin_branch="origin/master"):
    """Checks whether the active branch in the repo is
    up-to-date with origin_branch"""
    with git.Repo(local_clone) as r:
        # we're fetching the status for origin
        origin = r.remotes["origin"]
        origin.fetch()

        logging.debug(
            f"Current: {r.active_branch.commit}, remote: {r.refs[origin_branch].commit}"
        )
        return r.active_branch.commit != r.refs[origin_branch].commit


def check_and_update(local_clone, origin_branch="origin/master"):
    """Checks whether the active branch in the repo is
    up-to-date with origin_branch.
    It returns a boolean indicating whether an update was needed or not"""
    with git.Repo(local_clone) as r:
        # we're fetching the status for origin
        origin = r.remotes["origin"]
        origin.fetch()

        logging.debug(
            f"Current: {r.active_branch.commit}, remote: {r.refs[origin_branch].commit}"
        )
        needs_update = r.active_branch.commit != r.refs[origin_branch].commit

        if needs_update:
            origin.pull()
            gc(local_clone)

    return needs_update


def check_and_update_bare(local_clone):
    """Update a bare repository and return a boolean indicating whether
    an update was actually done"""
    with git.Repo(local_clone) as repo:
        # We create a set with all refs to check if there is actually a difference
        before = {
            (str(r.tag if hasattr(r, "tag") else r.commit), r.name) for r in repo.refs
        }
        logging.debug(f"before fetch {before}")
        repo.remotes.origin.fetch(
            prune=True,
            tags=True,
            force=True,
            prune_tags=True,
            refspec="+refs/*:refs/*",
        )
        after = {(str(r.commit), r.name) for r in repo.refs}
        logging.debug(f"after fetch {after}")
        return before != after


def repack(local_clone_path, max_pack_size_m=100):
    """Call git repack --max-pack-size=100m
    to limit the file size in the repository"""
    with git.Repo(local_clone_path) as repo:
        repo.git.repack(f"--max-pack-size={max_pack_size_m}m")


def gc(local_clone_path):
    """Call git  gc --auto on the repository"""
    with git.Repo(local_clone_path) as repo:
        repo.git.gc("--auto")
