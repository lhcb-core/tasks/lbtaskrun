###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from lbtaskrun import app
from lbtaskrun.cvmfs import git_update
from lbtaskrun.cvmfs.git import validate_gitcondb

REPO = "lhcb-condb.cern.ch"


@app.task(
    bind=True,
    name="lhcbcondb_git_update_lcg_toolchain",
    priority=100,
)
def git_update_lcg_toolchain(self):
    git_update(REPO, f"/cvmfs/{REPO}/lcg-toolchains")


@app.task(
    bind=True,
    name="lhcbcondb_git_update_lhcbstacks",
    priority=100,
)
def git_update_lhcbstacks(self):
    git_update(REPO, f"/cvmfs/{REPO}/lhcbstacks")


@app.task(bind=True, name="lhcbcondb_git_update_dddb", priority=100)
def git_update_dddb(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/DDDB.git",
        bare=True,
        validator=validate_gitcondb,
    )


@app.task(bind=True, name="lhcbcondb_git_update_dqflags", priority=100)
def git_update_dqflags(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/DQFLAGS.git",
        bare=True,
        validator=validate_gitcondb,
    )


@app.task(bind=True, name="lhcbcondb_git_update_lhcbcond", priority=100)
def git_update_lhcbcond(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/LHCBCOND.git",
        bare=True,
        validator=validate_gitcondb,
    )


@app.task(bind=True, name="lhcbcondb_git_update_online", priority=100)
def git_update_online(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/ONLINE.git",
        bare=True,
    )


@app.task(bind=True, name="lhcbcondb_git_update_simcond", priority=100)
def git_update_simcond(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/SIMCOND.git",
        bare=True,
        validator=validate_gitcondb,
    )


@app.task(bind=True, name="lhcbcondb_git_update_stcond", priority=100)
def git_update_stcond(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/STCOND.git",
        bare=True,
    )


@app.task(bind=True, name="lhcbcondb_git_update_velocond", priority=100)
def git_update_velocond(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/VELOCOND.git",
        bare=True,
    )


@app.task(bind=True, name="lhcbcondb_git_update_conditions", priority=100)
def git_update_conditions(self):
    git_update(
        REPO,
        f"/cvmfs/{REPO}/git-conddb/lhcb-conditions-database.git",
        bare=True,
    )


@app.task(bind=True, name="lhcbcondb_git_update_file_content_metadata", priority=100)
def git_update_file_content_metadata(self):
    git_update(REPO, f"/cvmfs/{REPO}/git-conddb/file-content-metadata.git", bare=True)


@app.task(bind=True, name="lhcbcondb_git_update_tcks", priority=100)
def git_update_tcks(self):
    git_update(REPO, f"/cvmfs/{REPO}/tcks.git", bare=True)
