###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from lbtaskrun import app, cvmfs

# List of installations possible on lhcbdev
# ##########################################

# [STARTED-deployLHCbDirac]
# [STARTED-installNightlies]
# [STARTED-manager_gc]
# [STARTED-manager_git]
# [STARTED-monitor]


@app.task(bind=True, track_started=True, name="lhcbdev_install_nightly", priority=50)
def install_nightly(self, slot_name, project_name, build_id, platforms):
    cvmfs.install_nightly(
        "lhcbdev.cern.ch",
        "/cvmfs/lhcbdev.cern.ch/nightlies",
        slot_name,
        project_name,
        build_id,
        platforms,
    )


@app.task(bind=True, track_started=True, name="lhcbdev_deploy_lhcbdirac", priority=250)
def deploy_lhcbdirac(self):
    cvmfs.deploy_lhcbdirac()
