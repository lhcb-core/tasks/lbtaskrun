###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from lbtaskrun import app, cvmfs

HOST_NAME = "lhcb.cern.ch"


@app.task(bind=True, name="lhcbprod_rsync_certificates", priority=200)
def rsync_certificates(self):
    return cvmfs.rsync_certificates(HOST_NAME, f"/cvmfs/{HOST_NAME}/etc/grid-security")


@app.task(bind=True, name="lhcbprod_update_platforms_list", priority=100)
def update_platforms_list(self):
    return cvmfs.update_platforms_list(
        HOST_NAME, f"/cvmfs/{HOST_NAME}/lib/var/lib/softmetadata/platforms-list.json"
    )


@app.task(
    bind=True, name="lhcbprod_update_project_platforms_from_lhcbstacks", priority=100
)
def update_project_platforms_from_lhcbstacks(self, pipeline_id):
    return cvmfs.update_project_platforms_from_lhcbstacks(
        HOST_NAME,
        f"/cvmfs/{HOST_NAME}/lib/var/lib/softmetadata/" "project-platforms.json",
        pipeline_id,
    )


@app.task(bind=True, name="lhcbprod_update_project_info_from_lhcbstacks", priority=100)
def update_project_info_from_lhcbstacks(self, pipeline_id):
    return cvmfs.update_project_info_from_lhcbstacks(
        HOST_NAME,
        f"/cvmfs/{HOST_NAME}/lib/var/lib/softmetadata/" "projects-info.json",
        pipeline_id,
    )


@app.task(bind=True, name="lhcbprod_update_lcg_info_from_lhcbstacks", priority=100)
def update_lcg_info_from_lhcbstacks(self, pipeline_id):
    return cvmfs.update_lcg_info_from_lhcbstacks(
        HOST_NAME,
        f"/cvmfs/{HOST_NAME}/lib/var/lib/softmetadata/" "lcg-info.json",
        pipeline_id,
    )


@app.task(bind=True, name="lhcbprod_create_lbenv", priority=100)
def create_lbenv(self):
    return cvmfs.create_lbenv(HOST_NAME, f"/cvmfs/{HOST_NAME}/lib/var/lib/LbEnv/")


@app.task(bind=True, name="lhcbprod_install_lhcbdirac", priority=150)
def install_lhcbdirac(self, version, diracos_version=None):
    return cvmfs.install_lhcbdirac(
        version,
        HOST_NAME,
        f"/cvmfs/{HOST_NAME}/lhcbdirac/",
        diracos_version=diracos_version,
    )


@app.task(bind=True, name="lhcbprod_deploy_lhcbpilot", priority=170)
def deploy_lhcbpilot(self):
    return cvmfs.deploy_lhcbdirac_pilot(
        HOST_NAME,
        f"/cvmfs/{HOST_NAME}/lhcbdirac/pilot/",
    )


@app.task(bind=True, name="lhcbprod_lhcbdirac_set_prod_link", priority=210)
def lhcbdirac_set_prod_link(self, version):
    return cvmfs.lhcbdirac_set_prod_link(
        version, HOST_NAME, f"/cvmfs/{HOST_NAME}/lhcbdirac/"
    )


@app.task(bind=True, track_started=True, name="lhcbprod_install_datapkg", priority=140)
def install_datapkg(self, rpm_fn, url):
    cvmfs.install_datapkg(rpm_fn, url, HOST_NAME, f"/cvmfs/{HOST_NAME}/lib/lhcb")


@app.task(
    bind=True,
    name="lhcbprod_deploy_conda_environment_v2",
    priority=220,
)
def deploy_conda_environment_v2(self, env_yaml, target_location, subdir):
    return cvmfs.deploy_conda_environment_v2(env_yaml, target_location, subdir)
