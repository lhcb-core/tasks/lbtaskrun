###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import asyncio
import logging
import os
import shlex
import shutil
import tempfile
from pathlib import Path

import celery

from lbtaskrun import utils
from lbtaskrun.cvmfs.transaction import CvmfsTransaction

COPY_BUFSIZE = 65_536


def run_in_apptainer(cmd, bind_ro=None, bind_rw=None, workdir=None, env=None):
    absolute_cmd = shutil.which(cmd[0])
    if not absolute_cmd:
        msg = f"Failed to find {cmd[0]} executable"
        raise RuntimeError(msg)
    cmd[0] = absolute_cmd
    bind_ro = (bind_ro or []) + [absolute_cmd]

    full_cmd = ["apptainer", "exec", "--containall"]
    full_cmd += [
        f"--bind={path if ':' in str(path) else f'{path}:{path}'}:ro"
        for path in bind_ro
    ]
    full_cmd += [
        f"--bind={path if ':' in str(path) else f'{path}:{path}'}"
        for path in bind_rw or []
    ]
    if workdir:
        full_cmd += [f"--bind={workdir}", f"--workdir={workdir}"]
    full_cmd += [f"--env={k}={v}" for k, v in (env or {}).items()]
    full_cmd += ["docker://centos:7"]
    full_cmd += cmd
    logging.warning("Running command: %s", shlex.join(full_cmd))
    ret = utils.logging_subprocess_run(full_cmd)
    ret.check_returncode()


def create_conda_env(prefix: Path, env_yaml: str, subdir: str):
    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_dir = Path(tmp_dir).absolute()
        workdir = tmp_dir / "work"
        workdir.mkdir()
        env_yaml_path = workdir / "environment.yaml"
        env_yaml_path.write_text(env_yaml)
        fake_prefix = workdir / "prefix"
        fake_prefix.mkdir()

        run_in_apptainer(
            [
                "micromamba",
                "create",
                "--quiet",
                "--yes",
                f"--root-prefix={tmp_dir}/root-prefix",
                f"--prefix={prefix}",
                f"--file={env_yaml_path}",
            ],
            bind_ro=[],
            bind_rw=[tmp_dir, f"{fake_prefix}:{prefix.parent}"],
            workdir=workdir,
            env={"CONDA_SUBDIR": subdir},
        )
        shutil.copytree(fake_prefix / prefix.name, prefix)


async def _copyfileobj(fsrc, fdst, length=0):
    """copy data from file-like object fsrc to file-like object fdst"""
    try:
        # Localize variable access to minimize overhead.
        fsrc_read = fsrc.read
        fdst_write = fdst.write
        fdst_drain = fdst.drain
        size_so_far = 0
        while buf := await fsrc_read(COPY_BUFSIZE):
            if not buf:
                break
            size_so_far += len(buf)
            fdst_write(buf)
            await fdst_drain()
    except ConnectionResetError:
        logging.exception("ConnectionResetError in _copyfileobj")
        return False
    else:
        logging.info(f"Transferred {size_so_far} bytes")
        return True


async def _stream_to_logging(name, stream):
    while line := await stream.readline():
        line = line.decode(errors="backslashreplace")
        logging.info(f"{name} {line.strip()}")


async def _ingest_zip_url(repo, base_dir, url, strip_prefix):
    cmd = ["cvmfs_server", "ingest", "--tar_file", "-", "--base_dir", base_dir, repo]
    logging.info(f"Running command {cmd}")
    p2 = await asyncio.create_subprocess_exec(
        *cmd,
        stdin=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        # Workaround for https://github.com/cvmfs/cvmfs/pull/2816
        env={**os.environ, "upstream_type": "gw"},
    )

    # Create a logger to stream stderr from the "cvmfs_server ingest" command
    loggers = [asyncio.create_task(_stream_to_logging("p2.stderr", p2.stderr))]

    # Check if the transaction was started successfully, retrying the task if not
    line = (await p2.stdout.readline()).decode(errors="backslashreplace")
    if line.strip() != "Gateway reply: ok":
        p2.stdin.close()
        line += (await p2.stdout.read()).decode(errors="backslashreplace")
        await p2.stdin.wait_closed()
        await p2.wait()
        logging.info(f"Unable to open transaction for cvmfs_server ingest: {line}")
        if celery.current_task:
            raise celery.current_task.retry(countdown=30) from None

    # Create a logger to stream stdout from the "cvmfs_server ingest" command
    loggers += [asyncio.create_task(_stream_to_logging("p2.stdout", p2.stdout))]

    # Create a subprocess to download the zip file and convert it to tar on stdout
    cmd = ["zip-to-tar", url]
    if strip_prefix:
        cmd += ["--strip-prefix", strip_prefix]
    p1 = await asyncio.create_subprocess_exec(
        *cmd,
        stdin=asyncio.subprocess.DEVNULL,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    loggers += [asyncio.create_task(_stream_to_logging("p1.stderr", p1.stderr))]

    # Stream the tar data to cvmfs_server ingest
    if not await _copyfileobj(p1.stdout, p2.stdin):
        msg = "Failed to stream tar data to cvmfs_server ingest"
        raise NotImplementedError(msg)

    try:
        await asyncio.wait_for(p1.wait(), timeout=30 * 60)
    except asyncio.TimeoutError:
        p1.kill()
    if p1.returncode != 0:
        logging.error("zip-to-tar failed, closing stdin for cvmfs_server")
        p2.stdin.close()

    logging.info("Finished streaming data, waiting for tasks to finish")
    await asyncio.gather(*loggers, p2.wait())

    if p1.returncode != 0 or p2.returncode != 0:
        msg = (
            "CVMFS ingestion exited with non-zero status "
            f"{p1.returncode=} {p2.returncode=}"
        )
        raise NotImplementedError(msg)
    logging.info("CVMFS ingestion was successful")


def install_nightly_zip(dest, url, *, strip_prefix=None):
    dest = Path(dest)

    # Ensure the current mount is up-to-date
    utils.logging_subprocess_run(["cvmfs_server", "tag", "-l"])

    # Make sure a suitable directory exists to lock from
    if not dest.exists():
        lock_prefix = dest
        while not lock_prefix.exists():
            lock_prefix = lock_prefix.parent

        lock_prefix /= dest.relative_to(lock_prefix).parts[0]
        logging.info(f"Creating prefix directory {dest} from {lock_prefix}")
        with CvmfsTransaction(lock_prefix.relative_to("/cvmfs")):
            os.makedirs(dest, exist_ok=True)

    # Do the actual installation
    repo = dest.relative_to("/cvmfs").parts[0]
    with CvmfsTransaction(str(dest.relative_to("/cvmfs")), start_transaction=False):
        return asyncio.get_event_loop().run_until_complete(
            _ingest_zip_url(
                repo, str(dest.relative_to(f"/cvmfs/{repo}")), url, strip_prefix
            )
        )
