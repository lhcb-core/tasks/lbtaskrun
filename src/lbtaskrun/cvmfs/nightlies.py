###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import functools
import logging
import os
from datetime import date, datetime, timedelta
from enum import Enum, auto
from pathlib import Path

import requests
from tzlocal import get_localzone

MIN_BUILDS_TO_KEEP = 5
DAYS_TO_KEEP = 7


# Util class to manage the links created
class Links(Enum):
    Today = auto()
    Yesterday = auto()
    latest = auto()


class Commands(Enum):
    remove = auto()
    link = auto()


def run(command):
    cmd = command[0]
    if cmd is Commands.remove:
        logging.warning(
            "Removing %s which pointed to %s", command[1], Path(command[1]).resolve()
        )
        os.remove(command[1])
    elif cmd is Commands.link:
        logging.warning("Linking %s to %s", command[2], command[1])
        if os.path.islink(command[2]):
            logging.warning("Path %s already exists! Removing...", command[2])
            os.remove(command[2])
        os.symlink(command[1], command[2])
    else:
        msg = f"Unknown command: {cmd!r}"
        raise NotImplementedError(msg)


def add_nightlies_link(slot_path, build_id, build_date=None, dryrun=False):
    """Add a link for a specific build for a specified slot"""

    # batch commands instead of running them straight away
    commands_to_run = []

    # Setting the Today and latest links
    today_path = os.path.join(slot_path, Links.Today.name)
    latest_path = os.path.join(slot_path, Links.latest.name)

    if os.path.exists(today_path):
        commands_to_run.append((Commands.remove, today_path))
    if os.path.exists(latest_path):
        commands_to_run.append((Commands.remove, latest_path))

    commands_to_run.append((Commands.link, str(build_id), today_path))
    commands_to_run.append((Commands.link, str(build_id), latest_path))

    # Update the link with the day name, if needed
    if build_date:
        day_name = datetime.strftime(build_date, "%a")
        day_path = os.path.join(slot_path, day_name)
        if os.path.exists(day_path):
            commands_to_run.append((Commands.remove, day_path))
        commands_to_run.append((Commands.link, str(build_id), day_path))

    # Now executing if needed
    if not dryrun:
        for c in commands_to_run:
            run(c)

    return commands_to_run


def _list_candidates_for_removal(slot_path: Path, time_cutoff: datetime) -> list[Path]:
    """Check builds in slot to see what to remove"""
    # First sort out the links and builds in the slot directory
    builds = set()
    linked_builds = set()
    n_kept = 0
    for fpath in slot_path.iterdir():
        if fpath.is_symlink():
            linked_builds.add(fpath.resolve())
        elif fpath.is_dir():
            # For safety only remove directories which are integers
            try:
                int(fpath.name)
            except ValueError:
                logging.debug(f"Not removing as build ID is not an int: {fpath}")
            else:
                ctime = datetime.fromtimestamp(
                    fpath.stat().st_mtime, tz=get_localzone()
                )
                if ctime < time_cutoff:
                    builds.add(fpath)
                else:
                    n_kept += 1

    # Now identifying the builds that can be removed as they are NOT pointed
    # by a link...
    eligible_to_remove = builds - linked_builds
    # Sort them by build ID
    eligible_to_remove = sorted(
        eligible_to_remove,
        key=lambda f: f.stat().st_ctime,
        reverse=True,
    )
    if n_kept < MIN_BUILDS_TO_KEEP:
        eligible_to_remove = eligible_to_remove[MIN_BUILDS_TO_KEEP - n_kept :]
    return eligible_to_remove


def get_nightlies_to_gc(nightlies_path) -> list[Path]:
    """Iterate over all the slots to see which builds should be removed"""
    nightlies_path = Path(nightlies_path)
    new_nightly_flavours = ["nightly", "testing", "release"]

    slots = list(nightlies_path.glob("*-*"))
    for flavour in new_nightly_flavours:
        slots += list((nightlies_path / flavour).glob("*-*"))

    time_cutoff = datetime.now(tz=get_localzone()) - timedelta(days=DAYS_TO_KEEP)

    toremove = []
    for slot_path in slots:
        toremove.extend(_list_candidates_for_removal(slot_path, time_cutoff))

    return toremove


@functools.lru_cache(maxsize=1024)
def get_build_date(slot_name, build_id):
    response = requests.get(
        f"https://lhcb-couchdb.cern.ch/nightlies-nightly/{slot_name}.{build_id}",
        verify=False,
        timeout=10,
    )
    response.raise_for_status()
    return date.fromisoformat(response.json()["date"])
