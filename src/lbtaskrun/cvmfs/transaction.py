###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
import os
import subprocess
import time
from contextlib import contextmanager
from datetime import datetime
from os.path import isfile, join

import celery

from lbtaskrun import utils

from .gateway import get_leases

logger = logging.getLogger(__name__)

# Lock file to disable new transactions to start on the repository
# This relies on the fact that all release managers share a HOME directory on CephFS
DISABLE_TRANSACTION_FULLPATH = "${HOME}/var/disable_transactions"
DISABLE_TRANSACTION_LOCALPATH = "/tmp/disable_transactions"
TRANSACTION_TIMESTAMP_PATH = "/tmp/lbtaskrun_transaction_timestamp"
PUBLISHING_TIMESTAMP_PATH = "/tmp/lbtaskrun_publishing_timestamp"


@contextmanager
def timestamp_file(filename):
    """Create a file for tracking when an operation started

    Currently used for tracking when transactions and publish operations started.
    """
    with open(filename, "w") as fp:
        fp.write(datetime.utcnow().isoformat())
    try:
        yield
    finally:
        if os.path.exists(filename):
            os.remove(filename)


def transactions_disabled(*, local=True, all=True):
    """
    Check whether the lockfile to disable transactions is present

    Set local=False or all=False to only restrict the lock file considered
    """
    if all and os.path.exists(os.path.expandvars(DISABLE_TRANSACTION_FULLPATH)):
        return True
    if local and os.path.exists(os.path.expandvars(DISABLE_TRANSACTION_LOCALPATH)):
        return True
    return False


def disable_transactions(reason="", *, all_release_managers=True):
    lockfile = os.path.expandvars(
        DISABLE_TRANSACTION_FULLPATH
        if all_release_managers
        else DISABLE_TRANSACTION_LOCALPATH
    )
    if transactions_disabled(local=not all_release_managers, all=all_release_managers):
        if celery.current_task:
            logger.warning(
                "Transactions are currently disabled, retrying task after a 10 second delay..."
            )
            # Sleep to avoid needlessly retrying tasks
            time.sleep(10)
            if celery.current_task.priority:
                raise celery.current_task.retry(
                    countdown=30, priority=max(celery.current_task.priority, 0)
                )
            else:
                raise celery.current_task.retry(countdown=300)
        msg = "Transactions already disabled"
        raise Exception(msg)
    lockdir = os.path.dirname(lockfile)
    if not os.path.exists(lockdir):
        os.makedirs(lockdir)
    with open(lockfile, "w") as f:
        f.write(reason)


def enable_transactions(*, all_release_managers=True):
    lockfile = os.path.expandvars(
        DISABLE_TRANSACTION_FULLPATH
        if all_release_managers
        else DISABLE_TRANSACTION_LOCALPATH
    )
    if not transactions_disabled(
        local=not all_release_managers, all=all_release_managers
    ):
        msg = "Transactions already enabled"
        raise Exception(msg)
    os.remove(lockfile)


class CvmfsLock:
    """Helper to deal with the lock on the filesystem"""

    def __init__(self, reason, *, all_release_managers=True):
        self.reason = reason
        self.all_release_managers = all_release_managers

    def __enter__(self):
        disable_transactions(
            self.reason, all_release_managers=self.all_release_managers
        )

    def __exit__(self, type, value, tb):
        enable_transactions(all_release_managers=self.all_release_managers)


class CvmfsAbortTransaction(Exception):
    """Specific exception to abort transactions"""


class CvmfsTransaction:
    """Helper to deal with CVMFS transactions.
    The repository passed can be either:
      - the repository name itself (e.g. lhcbdev.cern.ch) on a standard
        stratum-0
      - the repository followed by the path to lock (e.g.
        lhcbdev.cern.ch/nightlies/lhcb-head/22) on a multi gateway manager
    """

    def __init__(
        self,
        repository,
        retry=False,
        initial_delay=None,
        max_delay=None,
        max_retry=None,
        start_transaction=True,
        ignore_disabled=False,
    ):
        self.repository = str(repository)
        self.retry = retry
        self.initial_delay = initial_delay
        self.max_delay = max_delay
        self.max_retry = max_retry
        self.n_publish_retries = 5
        self.start_transaction = start_transaction
        self.ignore_disabled = ignore_disabled

    def __enter__(self):
        # Code to start a new transaction
        logger.info("starting transaction on %s" % self.repository)

        # Check if we have the lockfile on the shared file system
        if transactions_disabled(all=not self.ignore_disabled):
            logger.info("Transactions are currently disabled on %s" % self.repository)
            if celery.current_task:
                # Sleep to avoid needlessly retrying tasks
                time.sleep(10)
                if celery.current_task.priority:
                    raise celery.current_task.retry(
                        countdown=30, priority=max(celery.current_task.priority, 0)
                    )
                else:
                    raise celery.current_task.retry(countdown=300)
            msg = "Error starting CVMFS transaction: transactions disabled"
            raise Exception(msg)

        # Check if a lease has already been issued for the required path
        # This is used as it is much faster than trying to start the
        # transaction and then failing
        try:
            if celery.current_task and self.repository in get_leases(
                "http://gateway-cvmfs05.cern.ch:4929"
            ):
                logger.info(
                    "Retrying as a lease already exists for %r" % self.repository
                )
                msg = f"Lease already exists for {self.repository}"
                raise Exception(msg)
        except Exception:
            raise celery.current_task.retry(countdown=30) from None

        # Ensure the current mount is up-to-date
        utils.logging_subprocess_run(["cvmfs_server", "tag", "-l"])

        # Perform a sanity check for detecting broken release managers
        canary_file = join("/cvmfs", self.repository.split("/")[0], "README")
        if not isfile(canary_file):
            logger.warning(
                "Canary file %s does not exist! Waiting a minute and trying again...",
                canary_file,
            )
            time.sleep(60)
            # Ensure the current mount is up-to-date
            utils.logging_subprocess_run(["cvmfs_server", "tag", "-l"])
            if not isfile(canary_file):
                logger.error(
                    "Canary file %s still does not exist, disabling node!", canary_file
                )
                with open(DISABLE_TRANSACTION_LOCALPATH, "w") as fp:
                    fp.write(f"Canary file check with {canary_file} failed")
                utils.send_alarm(
                    f"Canary file check with {canary_file} has failed and the "
                    "release manager has been disabled by creating "
                    f"{DISABLE_TRANSACTION_LOCALPATH}. "
                    "This error must be recovered manually!"
                )
                raise celery.current_task.retry(countdown=30)

        if self.start_transaction:
            # Here are the cvmfs_server options:
            # transaction     [-r (retry if unable to acquire lease]
            #                 [-i INT (initial retry delay seconds)]
            #                 [-m INT (max retry delay seconds)]
            #                 [-n INT (max number of retries)]
            #                 <fully qualified name>
            command = ["cvmfs_server", "transaction"]
            if self.retry:
                command.append("-r")
            if self.initial_delay:
                command += ["-i", self.initial_delay]
            if self.max_delay:
                command += ["-m", self.max_delay]
            if self.max_retry:
                command += ["-n", self.max_retry]
            command.append(self.repository)

            logger.info("starting transaction with command %r" % command)
            ret = subprocess.run(command, check=False)
            if ret.returncode != 0:
                if celery.current_task:
                    raise celery.current_task.retry(countdown=30)
                msg = "Error starting CVMFS transaction"
                raise Exception(msg)

        self.transaction_timestamp = timestamp_file(TRANSACTION_TIMESTAMP_PATH)
        self.transaction_timestamp.__enter__()

    def __exit__(self, exc_type, exc_value, tb):
        self.transaction_timestamp.__exit__(exc_type, exc_value, tb)

        if exc_type is None:
            # The publish was handled externally (e.g. "cvmfs_server ingest")
            if not self.start_transaction:
                return None

            # No exception, so publish
            if celery.current_task:
                celery.current_task.update_state(state="CVMFS PUBLISH")

            with timestamp_file(PUBLISHING_TIMESTAMP_PATH):
                logger.info("Publishing transaction on %s" % self.repository)
                ret = utils.logging_subprocess_run(
                    ["cvmfs_server", "publish", self.repository]
                )
                if ret.returncode == 0:
                    return None
                else:
                    logger.error(
                        f"Failed to publish transaction on {self.repository} "
                        f"with code {ret.returncode}, aborting"
                    )
                    utils.send_alarm(
                        f"Failed to publish transaction {celery.current_task} "
                        f"with return code {ret.returncode}.\n\n"
                        f"    args: {ret.args}\n\n"
                        f"    returncode: {ret.returncode}\n\n"
                        f"    stdout: {ret.stdout}\n\n"
                        f"    stderr: {ret.stderr}\n"
                    )
                    # Avoid leaving the repository in an open transaction
                    self._abort()
                    time.sleep(60 * 5)
                    raise celery.current_task.retry(countdown=30)
        elif isinstance(exc_value, CvmfsAbortTransaction):
            # Exception occurred, so abort.
            logger.info("Aborting transaction on %s" % self.repository)
            ret = self._abort()
            if ret.returncode == 0:
                # The exception has been handled so suppress it
                return True

        # Other cases, the exception was not handled properly
        self._abort()
        return None

    def _abort(self):
        return subprocess.run(
            [
                "cvmfs_server",
                "abort",
                "-f",
                str(self.repository).split(os.path.sep)[0],
            ],
            check=False,
        )
