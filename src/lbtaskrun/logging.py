###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
import os
import ssl

import pika
from celery.app.log import TaskFormatter
from celery.signals import after_setup_logger
from python_logging_rabbitmq import RabbitMQHandler


@after_setup_logger.connect
def setup_loggers(logger, *args, **kwargs):
    sh = logging.StreamHandler()
    sh.setFormatter(
        TaskFormatter(
            "%(asctime)s - %(task_id)s - %(task_name)s - "
            "%(name)s - %(levelname)s - %(message)s"
        )
    )
    logger.addHandler(sh)

    if "LBAP_LOGGING_RMQ_HOST" in os.environ:
        logger.warning("Configuring centralised logging")
        rabbit_handler = RabbitMQHandler(
            host=os.environ["LBAP_LOGGING_RMQ_HOST"],
            port=os.environ["LBAP_LOGGING_RMQ_PORT"],
            username=os.environ["LBAP_LOGGING_RMQ_USER"],
            password=os.environ["LBAP_LOGGING_RMQ_PASSWORD"],
            declare_exchange=False,
            connection_params={
                "virtual_host": os.environ["LBAP_LOGGING_RMQ_VHOST"],
                "connection_attempts": 3,
                "socket_timeout": 5000,
                "ssl_options": pika.SSLOptions(
                    ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)
                ),
            },
        )
        # rabbit_handler.addFilter(CurrentTaskInfoFilter())
        logger.addHandler(rabbit_handler)
    else:
        logger.warning("No RabbitMQ credentials found, disabling")
