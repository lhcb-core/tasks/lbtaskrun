###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import hashlib
import logging
import os
import subprocess
from datetime import datetime, timezone
from pathlib import Path

import requests
from celery.worker.control import control_command, inspect_command

import lbtaskrun


@inspect_command()
def worker_version(state):
    logging.info("Got inspect command: worker_version()")
    return {"lbtaskrun": lbtaskrun.__version__}


@inspect_command()
def cvmfs_status(state):
    result = {
        "lock_file": None,
        "lock_file_contents": None,
    }

    # Lock files
    lock_fns = [
        lbtaskrun.cvmfs.transaction.DISABLE_TRANSACTION_LOCALPATH,
        lbtaskrun.cvmfs.transaction.DISABLE_TRANSACTION_FULLPATH,
    ]
    for fn in lock_fns:
        fn = os.path.expandvars(fn)
        if os.path.isfile(fn):
            result["lock_file"] = fn
            with open(fn) as fp:
                result["lock_file_contents"] = fp.read()
            break

    # Transaction status
    try:
        with open(lbtaskrun.cvmfs.transaction.TRANSACTION_TIMESTAMP_PATH) as fp:
            result["transaction_timestamp"] = fp.read()
    except Exception:
        result["transaction_timestamp"] = None
    try:
        with open(lbtaskrun.cvmfs.transaction.PUBLISHING_TIMESTAMP_PATH) as fp:
            result["publishing_timestamp"] = fp.read()
    except Exception:
        result["publishing_timestamp"] = None

    return result


@control_command(args=[("version", str)], signature="[version=master]")
def self_update(state, version="master"):
    logging.info("Got control command: self_update()")
    result = subprocess.run(
        ["update_lbtaskrun_environment.sh"], capture_output=True, check=False
    )
    logging.info("self_update stdout: " + result.stdout.decode())
    logging.info("self_update stderr: " + result.stderr.decode())
    logging.info("self_update: Shutting down worker")
    result.check_returncode()
    state["app"].control.shutdown(destination=[state["hostname"]])
    return {
        "args": result.args,
        "stdout": "OUTPUT DISABLED",  # result.stdout.decode(),
        "stderr": "OUTPUT DISABLED",  # result.stderr.decode(),
        "return_code": result.returncode,
    }


@lbtaskrun.app.task(bind=True, priority=200, name="task_update_workers")
def update_workers(self, version=None):
    worker_versions = lbtaskrun.all.app.control.inspect()._request("worker_version")
    # Workers with @celery-worker in the name are managed by openshift
    destinations = [host for host in worker_versions if "@celery-worker-" not in host]
    return lbtaskrun.app.control.broadcast(
        "self_update",
        arguments={"version": version} if version else {},
        destination=destinations,
        reply=True,
        timeout=300,
    )


@lbtaskrun.app.task(priority=50, name="task_alert_lbtaskrun_status")
def alert_lbtaskrun_status():
    webhook_url = os.environ["LBTASKS_STATUS_WEBHOOK_URL"]
    shared_dir = Path(os.environ.get("LBTASKS_SHARED_DIR", "/tmp"))
    cache_path = shared_dir / "mattermost-message.md5"

    r = requests.get("https://lhcb-core-tasks.web.cern.ch/overview/")
    r.raise_for_status()
    status = r.json()["status"]
    notes = r.json().get("notes", {})

    overall = status.pop("SUMMARY")
    to_ping = None
    if overall == 0:
        icon = ":ok_hand:"
    elif overall == 1:
        icon = ":warning:"
        to_ping = ""
    else:
        icon = ":alarm:"
        to_ping = "@cburr"

    body = [
        " ".join([icon] * 10),
        "",
        "Status by system:",
    ]
    hasher = hashlib.md5()
    if overall != 0:
        # Use a cache key that messages are repeated if errors persist
        ct = datetime.now(timezone.utc)
        hasher.update(f"{ct.year}-{ct.month}-{ct.day} {ct.hour // 6}".encode())

    for name, state in sorted(status.items()):
        if state == 0:
            line = f" * {name}: :ok_hand: All is okay"
        elif state == 1:
            line = f" * {name}: :warning: There are warnings"
        else:
            line = f" * {name}: :alarm: There is a serious issue"
        body.append(line)
        hasher.update(line.encode())
        body.extend(f"   * {x}" for x in notes.get(name, []))
        hasher.update(f"{len(notes)}".encode())

    if to_ping:
        body.append(f"\ncc {to_ping}")

    body = "\n".join(body)

    if cache_path.exists() and cache_path.read_text() == hasher.hexdigest():
        return "No change in status"

    r = requests.post(
        webhook_url, json={"text": body}, headers={"Content-Type": "application/json"}
    )
    r.raise_for_status()
    cache_path.write_text(hasher.hexdigest())
    return "Posted to Mattermost"
