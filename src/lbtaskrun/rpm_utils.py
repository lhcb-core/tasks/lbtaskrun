###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Simple tool that lists the RPMs that should be installed for a package of project,
taking the project

"""

from __future__ import annotations

import os
import re
import subprocess
import sys
import tempfile
from os.path import join

import click
import requests

from lbtaskrun import utils


@click.command()
@click.argument("build_id", type=int)
def printRPMsFromRelease(build_id):
    names = list_rpms_in_build(build_id)
    print("\n".join(names))


def _get_build_info(build_id):
    """Lists the RPMs in the build by name"""

    url = f"https://lhcb-couchdb.cern.ch/nightlies-release/lhcb-release.{build_id}"
    try:
        response = requests.get(url)
    except requests.exceptions.SSLError:
        # FIXME: it's possible that we do not have the right certificates
        response = requests.get(url, verify=False)

    response.raise_for_status()
    return response.json()


def list_rpms_in_build(build_id):
    """Lists the RPMs in the build by name"""

    build_info = _get_build_info(build_id)

    config = build_info["config"]
    suffixes = ["", "_index"] + [
        "_" + platform.replace("-", "_") for platform in config["platforms"]
    ]
    names = [
        "{name}_{version}{suffix}".format(
            name=project["name"].upper(), version=project["version"], suffix=suffix
        )
        for project in config["projects"]
        for suffix in suffixes
        if not project.get("disabled")
    ]
    names.extend(
        project["name"].upper() + "_shared"
        for project in config["projects"]
        if project.get("with_shared")
    )
    names.sort()
    return names


def get_build_artefact_url(build_id, rpm_name):
    """Return the URL for a specific RPM, to be used with
    the names returned by list_rpms_in_build"""
    prefix = "https://lhcb-nightlies-artifacts.web.cern.ch/"
    +f"release/lhcb-release/{build_id}/rpms"
    filename = get_rpm_fullname(rpm_name)
    return "/".join([prefix, filename])


def get_rpm_fullname(rpm_name):
    """Suffix the RPM name with the default version and release"""
    # XXX We cannot guess if version number so it is hardcoded for the moment
    return f"{rpm_name}-1.0.0-1.noarch.rpm"


def parse_fn(fn):
    RPM_FN_PATTERN = (
        r"(DBASE|PARAM)_"
        r"((?:[A-Za-z0-9]{1,100}_)?[A-Za-z0-9]{1,100})_"
        r"(v\d{1,20}r\d{1,20}(?:p\d{1,20}(?:t\d{1,20})?)?)-"
        r"1\.0\.0-(\d{1,4})"
        r"\.noarch\.rpm"
    )
    match = re.fullmatch(RPM_FN_PATTERN, fn)
    if not match:
        msg = f"Failed to parse RPM filename: {fn}"
        raise ValueError(msg)
    rpm_project, rpm_pkg_name, rpm_pkg_version, rpm_build_id = match.groups()
    return rpm_project, rpm_pkg_name, rpm_pkg_version, rpm_build_id


def add_url_to_repo(fn, url):
    # Upload the RPM to the S3 based repository
    with tempfile.TemporaryDirectory() as tmpdirname:
        local_rpm_fn = os.path.join(tmpdirname, fn)

        # Download the RPM
        with requests.get(url, stream=True, timeout=10) as r:
            r.raise_for_status()
            with open(local_rpm_fn, "wb") as fp:
                for chunk in r.iter_content(chunk_size=8192):
                    fp.write(chunk)

        ret = utils.logging_subprocess_run(["createrepo_c", tmpdirname], cwd=tmpdirname)
        if ret.returncode != 0:
            msg = "Error in createrepo_c"
            raise utils.LbTaskRunSubprocessError(msg, ret)

        # Merge the local repository with the remote one
        ret = utils.logging_subprocess_run(
            [
                "mergerepo_c",
                "--repo=http://lhcb-rpm.s3.cern.ch/lbtaskrun/",
                "--omit-baseurl",
                "--database",
                "--repo",
                tmpdirname,
            ],
            cwd=tmpdirname,
        )
        if ret.returncode != 0:
            msg = "Error in mergerepo_c"
            raise utils.LbTaskRunSubprocessError(msg, ret)

        # Upload the RPM to S3
        ret = utils.logging_subprocess_run(
            [
                "rclone",
                "copyto",
                "--s3-no-check-bucket",
                local_rpm_fn,
                f"cern-s3:lhcb-rpm/lbtaskrun/{fn}",
                "--s3-acl",
                "public-read",
            ]
        )
        if ret.returncode != 0:
            msg = "Error uploading RPM"
            raise utils.LbTaskRunSubprocessError(msg, ret)

        # Upload the repodata to S3
        ret = utils.logging_subprocess_run(
            [
                "rclone",
                "sync",
                join(tmpdirname, "merged_repo/repodata/"),
                "cern-s3:lhcb-rpm/lbtaskrun/repodata/",
                "--s3-acl",
                "public-read",
            ]
        )
        if ret.returncode != 0:
            msg = "Error uploading RPM"
            raise utils.LbTaskRunSubprocessError(msg, ret)


def rpms_in_build_by_platform(build_id):
    """Lists the RPMs in the build by name"""

    build_info = _get_build_info(build_id)

    config = build_info["config"]

    # We keep one list of RPMs per suffix, i.e. per platform or either
    # _index or source RPMs
    packages = {}
    suffixes = ["", "_index"] + [
        "_" + platform.replace("-", "_") for platform in config["platforms"]
    ]
    # Dealing with all suffixes
    for suffix in suffixes:
        names = [
            "{name}_{version}{suffix}".format(
                name=project["name"].upper(), version=project["version"], suffix=suffix
            )
            for project in config["projects"]
            if not project.get("disabled")
        ]
        if suffix:
            packages[suffix] = sorted(names)
        else:
            packages["source"] = sorted(names)
    # We also have "shared" packages
    shared = [
        project["name"].upper() + "_shared"
        for project in config["projects"]
        if project.get("with_shared")
    ]
    packages["shared"] = sorted(shared)
    return packages


def install_commands(rpms):
    """Returns a list of commands to be invoked to install the packages
    including the transaction"""
    if len(rpms) == 0:
        return []
    cmds = [["lbcvmfs_transaction"]]
    cmds.append(["lbinstall", "install", *rpms])
    cmds.append(["lbcvmfs_publish"])
    return cmds


@click.command()
@click.argument("build_id", type=int)
@click.option("--confirm", is_flag=True, default=False, help="Confirm the installation")
def installReleaseInBatch(build_id, confirm):
    # First get the whole list of packages to install
    packages = rpms_in_build_by_platform(build_id)

    # Now establish the list of commands to run to deploy platforms one by one
    # The source, _index and shared packages are special, we install them then first
    special_platforms = ["source", "_index", "shared"]
    all_commands = {}
    for p in special_platforms:
        all_commands[p] = install_commands(packages[p])

    # standard platforms are all others
    platforms = set(packages.keys()) - set(special_platforms)
    for p in platforms:
        all_commands[p] = install_commands(packages[p])

    if not confirm:
        print("In dry run mode, listing the commands to be executed")
        print("Use --confirm to perform the installation")
        for p, platform_commands in all_commands.items():
            print(f"\nListing commands for platform: {p}")
            for c in platform_commands:
                print(" ".join(c))
    else:
        print(f"In installation mode ({len(all_commands)} platforms to install)")
        counter = 1
        for p, platform_commands in all_commands.items():
            print(f"\nInstalling platform: {p} ({counter} out of {len(all_commands)})")
            for c in platform_commands:
                print(f"Running: {c}")
                result = subprocess.run(c, check=True)
                if result.returncode != 0:
                    print(
                        f"Error running: {c}, Aborting. Please check transaction status and call lbcvmfs_abort if necessary"
                    )
                    sys.exit(1)
