###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os
from subprocess import CalledProcessError

from requests.exceptions import HTTPError
from sentry_sdk.integrations import Integration, celery
from sentry_sdk.scope import add_global_event_processor

from .utils import LbTaskRunSubprocessError


class LbTaskRunIntegration(Integration):
    """Capture stderr and stdout from CalledProcessError exceptions."""

    identifier = "lbtaskrun"

    @staticmethod
    def setup_once():
        @add_global_event_processor
        def capture(event, hint):
            if "exc_info" not in hint:
                return event

            _, e, _ = hint["exc_info"]

            called_proc_error = None
            if isinstance(e, LbTaskRunSubprocessError):
                called_proc_error = e.exc
            if isinstance(e, CalledProcessError):
                called_proc_error = e
            if called_proc_error:
                breadcrumbs = event.get("breadcrumbs")
                if not breadcrumbs:
                    return event

                data = breadcrumbs[-1].setdefault("data", {})
                for key in ["stderr", "stdout"]:
                    value = getattr(called_proc_error, key)
                    if isinstance(value, bytes):
                        data[key] = value.decode(errors="backslashreplace")
                    else:
                        data[key] = value
            return event


class RequestsIntegration(Integration):
    """Sentry integration to capture detail about HTTP errors from requests."""

    identifier = "requests"

    @staticmethod
    def setup_once():
        @add_global_event_processor
        def capture(event, hint):
            if "exc_info" not in hint:
                return event

            _, e, _ = hint["exc_info"]
            if not isinstance(e, HTTPError):
                return event

            breadcrumbs = event.get("breadcrumbs")
            if not breadcrumbs:
                return event

            data = breadcrumbs[-1].setdefault("data", {})
            data["response"] = e.response.text

            return event


def init():
    import sentry_sdk

    integrations = [
        celery.CeleryIntegration(),
        LbTaskRunIntegration(),
        RequestsIntegration(),
    ]
    try:
        from sentry_sdk.integrations import flask
    except ImportError:
        pass
    else:
        integrations.append(flask.FlaskIntegration())

    sentry_sdk.init(os.environ["LBTASKS_SENTRY_DSN"], integrations=integrations)
