#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os
import socket
import ssl
from concurrent.futures import ThreadPoolExecutor, as_completed
from dataclasses import dataclass
from datetime import datetime, timedelta
from pathlib import Path

from cryptography import x509
from cryptography.x509.oid import NameOID

X509_CERT_DIR_LOCATIONS = [
    "/etc/grid-security/certificates/",
    "/cvmfs/lhcb.cern.ch/etc/grid-security/certificates/",
]
if "X509_CERT_DIR" in os.environ:
    X509_CERT_DIR_LOCATIONS.insert(0, os.environ["X509_CERT_DIR"])
for X509_CERT_DIR in map(Path, X509_CERT_DIR_LOCATIONS):
    if X509_CERT_DIR.exists() and list(X509_CERT_DIR.iterdir()):
        cern_ctx = ssl.create_default_context(capath=X509_CERT_DIR)
        cern_ctx.check_hostname = True
        cern_ctx.verify_mode = ssl.CERT_REQUIRED
        # cern_ctx.verify_flags |= ssl.VERIFY_CRL_CHECK_CHAIN
        cern_ctx.load_default_certs()
        break
else:
    cern_ctx = None

public_ctx = ssl.create_default_context()
public_ctx.check_hostname = True
public_ctx.verify_mode = ssl.CERT_REQUIRED
# public_ctx.verify_flags |= ssl.VERIFY_CRL_CHECK_CHAIN
public_ctx.load_default_certs()

unsafe_ctx = ssl.create_default_context()
unsafe_ctx.check_hostname = False
unsafe_ctx.verify_mode = ssl.CERT_NONE


@dataclass
class HostInfo:
    cert: x509.Certificate
    hostname: tuple
    port: int
    peername: str | None
    public_error: str | None
    cern_error: str | None

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.cert, self.peername = _get_cert(unsafe_ctx, hostname, port)

        self.public_error = None
        try:
            _get_cert(public_ctx, hostname, port)
        except Exception as e:
            self.public_error = str(e)

        if cern_ctx:
            self.cern_error = None
            try:
                _get_cert(cern_ctx, hostname, port)
            except Exception as e:
                self.cern_error = str(e)
        else:
            self.cern_error = "Unable to find directory of CAs"

    def __repr__(self):
        return f"{self.hostname}:{self.port}"

    def __str__(self):
        info = [
            f"» {self.hostname}:{self.port} « {self.peername}",
            f"\tcommonName: {self.common_name}",
            f"\tSAN: {self.alt_names}",
            f"\tissuer: {self.issuer}",
            f"\tnotBefore: {self.not_valid_before}",
            f"\tnotAfter:  {self.not_valid_after}",
            f"\tvalid: {self.valid}",
            f"\tpublic_error: {self.public_error}",
            f"\tcern_error: {self.cern_error}",
            f"\tneeds renewing: {self.time_left < timedelta(days=14)}",
        ]
        return "\n".join(info)

    @property
    def not_valid_before(self):
        return self.cert.not_valid_before

    @property
    def not_valid_after(self):
        return self.cert.not_valid_after

    @property
    def time_left(self):
        return self.not_valid_after - datetime.now()

    @property
    def alt_names(self):
        try:
            ext = self.cert.extensions.get_extension_for_class(
                x509.SubjectAlternativeName
            )
            return ext.value.get_values_for_type(x509.DNSName)
        except x509.ExtensionNotFound:
            return None

    @property
    def common_name(self):
        try:
            names = self.cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
            return names[0].value
        except x509.ExtensionNotFound:
            return None

    @property
    def issuer(self):
        try:
            names = self.cert.issuer.get_attributes_for_oid(NameOID.COMMON_NAME)
            return names[0].value
        except x509.ExtensionNotFound:
            return None

    @property
    def valid(self):
        return not (self.public_error and self.cern_error)


def _get_cert(ctx, hostname, port):
    with socket.create_connection((hostname, port), 1) as sock:
        peername = sock.getpeername()
        with ctx.wrap_socket(sock, server_hostname=hostname) as sock_ssl:
            sock_ssl.do_handshake()
            crypto_cert = x509.load_der_x509_certificate(
                sock_ssl.getpeercert(binary_form=True)
            )
    return crypto_cert, peername


def check_for_errors():
    data = Path(os.environ["LB_SSL_HOSTS_FN"]).read_text()
    hosts = [
        line.split(":")
        for line in data.splitlines()
        if line and not line.strip().startswith("#")
    ]
    hosts = [(h, int(p)) for h, p in hosts][2:]

    hostinfos = []
    unknown_errors = {}
    with ThreadPoolExecutor(max_workers=2) as e:
        futures = {
            e.submit(HostInfo, hostname, port): (hostname, port)
            for hostname, port in hosts
        }
        for future in as_completed(futures):
            hostname, port = futures[future]
            if e := future.exception():
                unknown_errors[f"{hostname}:{port}"] = str(e)
            else:
                hostinfos += [future.result()]

    errors = {
        "invalid": [f"{h.hostname}:{h.port}" for h in hostinfos if not h.valid],
        "expiring_soon": [
            f"{h.hostname}:{h.port}"
            for h in hostinfos
            if h.time_left < timedelta(days=14)
        ],
        "unknown_errors": unknown_errors,
    }

    return hostinfos, errors


if __name__ == "__main__":
    hostinfos, errors = check_for_errors()
    for hostinfo in hostinfos:
        print(hostinfo)
    print(errors)
