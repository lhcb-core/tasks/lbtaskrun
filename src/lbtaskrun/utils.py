###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
import os
import smtplib
import socket
import subprocess
from concurrent.futures import ThreadPoolExecutor
from email.message import EmailMessage

import click
import requests

logger = logging.getLogger(__name__)


class LbTaskRunSubprocessError(Exception):
    def __init__(self, msg, exc):
        self.msg = msg
        self.exc = exc
        super().__init__(msg, called_process_to_json(exc))

    def __reduce__(self):
        return self.__class__, (self.msg, self.exc)


def called_process_to_json(ret):
    # The schema for the celery results backend only support 64KB responses
    # There is a open PR to increase this to 4GB but regardless probably don't
    # want to fill the database with so much junk
    # * https://github.com/celery/celery/pull/5771
    stdout = ret.stdout.decode()
    if len(stdout) > 20000:
        logger.warning(
            f"Command stdout from {ret.args} is too long "
            f"({len(stdout)}) truncating to 20000 characters"
        )
        stdout = stdout[-20000:]

    stderr = ret.stderr.decode()
    if len(stderr) > 20000:
        logger.warning(
            f"Command stderr from {ret.args} is too long "
            f"({ret.stderr}) truncating to 20000 characters"
        )
        stderr = stderr[-20000:]

    return {
        "args": ret.args,
        "returncode": ret.returncode,
        "stdout": stdout,
        "stderr": stderr,
    }


def log_popen_pipe(p, pipe_name):
    result = b""
    while p.poll() is None:
        line = getattr(p, pipe_name).readline()
        result += line
        try:
            line = line.decode().strip()
        except UnicodeDecodeError:
            line = repr(line)
        if line:
            logger.info(f"cmd {pipe_name}: {line}")
    return result


def logging_subprocess_run(args, *, cwd=None, shell=False):
    kwargs = {"cwd": cwd, "stdout": subprocess.PIPE, "stderr": subprocess.PIPE}
    if shell:
        kwargs["shell"] = shell
        kwargs["executable"] = "/bin/bash"
    with subprocess.Popen(args, **kwargs) as p, ThreadPoolExecutor(2) as pool:
        stdout = pool.submit(log_popen_pipe, p, "stdout")
        stderr = pool.submit(log_popen_pipe, p, "stderr")
        stdout = stdout.result()
        stderr = stderr.result()
    return subprocess.CompletedProcess(args, p.returncode, stdout, stderr)


def get_cern_certs_location():
    """Try to find a path contain CERN's CAs"""
    paths_to_try = [
        "/etc/grid-security/certificates",
        "/cvmfs/lhcb.cern.ch/etc/grid-security/certificates",
    ]
    for path in paths_to_try:
        if os.path.exists(path):
            return path
    msg = "Failed to find an available location"
    raise FileNotFoundError(msg)


def tarfile_strip_prefix(members, prefix):
    for tarinfo in members:
        if not tarinfo.path.startswith(prefix):
            raise NotImplementedError(tarinfo.path)
        tarinfo.path = tarinfo.path.replace(prefix, ".")
        yield tarinfo


def send_alarm(message):
    try:
        msg = EmailMessage()
        msg.set_content(message)
        msg["Subject"] = f"Error when publishing transaction on {socket.gethostname()}"
        msg["From"] = "lbtaskrun@cern.ch"
        msg["To"] = ["christopher.burr@cern.ch"]

        with smtplib.SMTP("cernmx.cern.ch", timeout=2) as smtp:
            smtp.send_message(msg)
    except Exception:
        logger.exception("Failed to send alarm email")


@click.command()
@click.argument("pipeline_id", type=int)
def cmd_get_pipeline_project_platforms(pipeline_id):
    get_pipeline_project_platforms(pipeline_id)


def get_pipeline_project_platforms(pipeline_id):
    """Get the projects-platforms.json file for a given pipeline"""

    # Utils to form the URL
    server = "https://gitlab.cern.ch"
    project = f"api/v4/projects/{LHCBSTACKS_PROJECT_ID}"

    def get_url(x):
        return "/".join([server, project, x])

    # Find the ID of the job that generated the project platforms JSON
    pipeline_jobs = f"pipelines/{pipeline_id}/jobs"
    response = requests.get(
        get_url(pipeline_jobs), verify=get_cern_certs_location(), timeout=10
    )
    response.raise_for_status()
    jobs = response.json()
    jobid = None
    for j in jobs:
        if j["name"] == "generate_projects_platforms":
            jobid = j["id"]
    if jobid is None:
        msg = f"Could not find generate_projects_platforms job for {pipeline_id}"
        raise Exception(msg)

    # Get the output for the job
    artefact = f"jobs/{jobid}/artifacts/projects_platforms.json"
    response = requests.get(
        get_url(artefact), verify=get_cern_certs_location(), timeout=10
    )
    response.raise_for_status()
    return response.json()


def get_pipeline_artifact(pipeline_id, project_id, job_name, artifact_name):
    """Get the artifact for a specific job for a given pipeline"""

    # Utils to form the URL
    server = "https://gitlab.cern.ch"
    project = f"api/v4/projects/{project_id}"

    def get_url(x):
        return "/".join([server, project, x])

    # Find the ID of the job that generated the project platforms JSON
    pipeline_jobs = f"pipelines/{pipeline_id}/jobs"
    response = requests.get(
        get_url(pipeline_jobs), verify=get_cern_certs_location(), timeout=10
    )
    response.raise_for_status()
    jobs = response.json()
    jobid = None
    for j in jobs:
        if j["name"] == job_name:
            jobid = j["id"]
    if jobid is None:
        msg = f"Could not find {job_name} job for {pipeline_id}"
        raise Exception(msg)

    # Get the output for the job
    artefact = f"jobs/{jobid}/artifacts/{artifact_name}"
    response = requests.get(
        get_url(artefact), verify=get_cern_certs_location(), timeout=10
    )
    response.raise_for_status()
    return response.json()


LHCBSTACKS_PROJECT_ID = "110853"


def get_pipeline_project_info(pipeline_id):
    """The project info file from the lhcbstacks"""
    return get_pipeline_artifact(
        pipeline_id,
        LHCBSTACKS_PROJECT_ID,
        "generate_projects_info",
        "projects_info.json",
    )


def get_pipeline_lcg_info(pipeline_id):
    """The lcg info file from the lhcbstacks"""
    return get_pipeline_artifact(
        pipeline_id, LHCBSTACKS_PROJECT_ID, "generate_lcg_info", "lcg_info.json"
    )
