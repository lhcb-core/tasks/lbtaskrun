#! /usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import argparse
import io
import os
import resource
import stat
import sys
import tarfile
import time
from zipfile import ZipFile

import requests


def set_memory_limit(limit_bytes):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    if 0 < soft < limit_bytes:
        sys.stderr.write(
            f"WARNING: Tried to new soft limit ({limit_bytes}) above "
            f"current limit ({soft}), reducing...\n"
        )
    else:
        soft = limit_bytes
    if 0 < hard < limit_bytes:
        sys.stderr.write(
            f"WARNING: Tried to new hard limit ({limit_bytes}) above "
            f"current limit ({hard}), reducing...\n"
        )
    else:
        hard = limit_bytes
    sys.stderr.write(
        f"Setting RLIMIT_AS to be ({soft // 1024**3:.2f}, {hard // 1024**3:.2f}) GB\n"
    )
    resource.setrlimit(resource.RLIMIT_AS, (soft, hard))


def zip_to_tar(input_file, output_stream, *, strip_prefix=None, verbose=False):
    if output_stream.isatty():
        msg = "Exiting as stdout is not connected to a pipe"
        raise NotImplementedError(msg)

    strip_prefix = os.path.normpath(strip_prefix) + "/" if strip_prefix else ""
    strip_prefix_len = len(strip_prefix)

    with ZipFile(input_file) as zipf, tarfile.open(
        fileobj=output_stream, mode="w|"
    ) as tarf:
        extracted, skipped, total = 0, 0, len(zipf.infolist())
        sys.stderr.write(f"Found {total} files to extract\n")
        for i, zip_info in enumerate(zipf.infolist(), start=1):
            if i % 1000 == 0:
                sys.stderr.write(f"Processing file {i} of {total}\n")

            filename = zip_info.filename[strip_prefix_len:]
            if not (zip_info.filename.startswith(strip_prefix) and filename):
                skipped += 1
                if verbose:
                    sys.stderr.write(f"Skipping: {zip_info.filename}\n")
                continue
            if verbose:
                sys.stderr.write(f"{zip_info.filename} -> {filename}\n")

            tar_info = tarfile.TarInfo(name=zip_info.filename[strip_prefix_len:])
            tar_info.size = zip_info.file_size
            tar_info.mtime = time.mktime((*tuple(zip_info.date_time), -1, -1, -1))
            tar_info.uid = os.getuid()
            tar_info.gid = os.getgid()
            s_mode = zip_info.external_attr >> 16
            tar_info.mode = s_mode & 0o777
            fileobj = zipf.open(zip_info.filename)
            if stat.S_ISLNK(s_mode):
                tar_info.type = tarfile.SYMTYPE
                tar_info.size = 0
                tar_info.linkname = fileobj.read().decode()
                fileobj = None

            tarf.addfile(tarinfo=tar_info, fileobj=fileobj)
            extracted += 1
        sys.stderr.write(
            f"All files extracted successfully ({extracted=}, {skipped=}, {total=})\n"
        )


def url_to_tar(input_url, output_stream, *, strip_prefix=None, verbose=False):
    # Ensure this process cannot consume more than 12 GB of RAM
    set_memory_limit(12 * 1024**3)
    sys.stderr.write(f"Downloading file from {input_url}\n")
    r = requests.get(input_url)

    sys.stderr.write(
        f"Downloaded {os.path.basename(input_url)}: {r.status_code} {r.reason} "
        f"with size {len(r.content) / 1024**3:.2f} GB\n"
    )
    r.raise_for_status()
    ifn = io.BytesIO(r.content)

    zip_to_tar(ifn, output_stream, strip_prefix=strip_prefix, verbose=verbose)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("url")
    parser.add_argument("output", default="-", nargs="?")
    parser.add_argument("--strip-prefix")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    output = args.output
    output = sys.stdout.buffer if not output or output == "-" else open(output, "wb")

    converter = zip_to_tar if os.path.exists(args.url) else url_to_tar
    try:
        converter(
            args.url, output, strip_prefix=args.strip_prefix, verbose=args.verbose
        )
    finally:
        output.close()


if __name__ == "__main__":
    parse_args()
