###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os

import pytest


@pytest.fixture()
def cvmfs_mock(monkeypatch):
    class CvmfsMock:
        def __init__(self, *args, **kwargs):
            self.returncode = 0

    # Change the environment
    monkeypatch.setattr(
        "lbtaskrun.cvmfs.transaction.CvmfsTransaction.__enter__", CvmfsMock
    )
    monkeypatch.setattr(
        "lbtaskrun.cvmfs.transaction.CvmfsTransaction.__exit__", lambda *a, **kw: None
    )

    return CvmfsMock


@pytest.fixture()
def fake_cvmfs_canary(monkeypatch):
    def IsFileMock(*args, **kwargs):
        print(f"isfile {args}")
        if args[0].endswith("README"):
            return True
        else:
            return os.path.isfile(*args, **kwargs)

    monkeypatch.setattr("lbtaskrun.cvmfs.transaction.isfile", IsFileMock)
