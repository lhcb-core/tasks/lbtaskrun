###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import json

import pytest
import responses

from lbtaskrun.cvmfs.gateway import (
    get_leases,
    get_leases_url,
    wait_until_transactions_done,
)

TEST_SERVER = "http://test.server.lhcb"
GATEWAY_WITH_LEASE = """{
  "data": {
    "lhcbdev.cern.ch/test": {
      "key_id": "lhcbdev.cern.ch",
      "expires": "2020-03-24 22:32:34.793159112 +0100 CET"
    },
    "lhcbdev.cern.ch/titi": {
      "key_id": "lhcbdev.cern.ch",
      "expires": "2020-03-24 22:29:25.734463685 +0100 CET"
    }
  },
  "status": "ok"
}
 """

GATEWAY_NO_LEASE = """ {
  "data": { },
  "status": "ok"
}
"""


@pytest.fixture()
def mocked_responses():
    with responses.RequestsMock() as rsps:
        for _ in range(3):
            rsps.add(
                responses.GET,
                get_leases_url(TEST_SERVER),
                body=GATEWAY_WITH_LEASE,
                status=200,
                content_type="application/json",
            )

        for _ in range(2):
            rsps.add(
                responses.GET,
                get_leases_url(TEST_SERVER),
                body=GATEWAY_NO_LEASE,
                status=200,
                content_type="application/json",
            )

        yield rsps


def test_wait(mocked_responses):
    leases = json.loads(GATEWAY_WITH_LEASE)
    assert get_leases(TEST_SERVER) == leases["data"]
    wait_until_transactions_done(TEST_SERVER, 1.0)
    leases = json.loads(GATEWAY_NO_LEASE)
    assert get_leases(TEST_SERVER) == leases["data"]
