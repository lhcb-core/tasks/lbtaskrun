###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os

import git
import pytest

from lbtaskrun.cvmfs import git_update
from lbtaskrun.cvmfs.git import (
    check_and_update,
    check_and_update_bare,
    gc,
    gitcondb_tag_valid,
    local_clone_needs_update,
    repack,
)


def update_bare_repo(tmp_path, bare_repo_path):
    """Add a commit to a file in a bare repo"""
    repo_dir = os.path.join(tmp_path, "update-git-repo")
    file_name = os.path.join(repo_dir, "test_file")

    origin = git.Repo(bare_repo_path)
    r = origin.clone(repo_dir)

    r = git.Repo.init(repo_dir, initial_branch="master")
    with open(file_name, "w") as f:
        f.write("version3")
    r.index.add([file_name])
    r.index.commit("Update")
    r.remotes.origin.push()


def clone_bare_repo(tmp_path, bare_repo):
    """Clones a bare repo with the mirror option for tests"""
    bare_path = os.path.join(tmp_path, "bare_clone")
    os.mkdir(bare_path)
    bare_repo.clone(bare_path, multi_options=["--mirror"])
    bare_repo.remotes.origin.fetch()
    return bare_path


@pytest.fixture()
def simple_git_repo(tmp_path):
    """Fixture to create a custom directory"""
    repo_dir = os.path.join(tmp_path, "test-git-repo")
    file_name = os.path.join(repo_dir, "test_file")

    repo = git.Repo.init(repo_dir, initial_branch="master")
    with open(file_name, "w") as f:
        f.write("version1")
    repo.index.add([file_name])
    repo.index.commit("initial commit")
    with open(file_name, "w") as f:
        f.write("version2")
    repo.index.add([file_name])
    repo.index.commit("Updated to v2")

    return repo


@pytest.fixture()
def bare_git_repo(tmp_path, simple_git_repo):
    bare_path = os.path.join(tmp_path, "bare_repo")
    os.mkdir(bare_path)
    repo = simple_git_repo
    repo.clone(bare_path, multi_options=["--mirror"])
    return bare_path


def test_clone_uptodate(tmp_path, simple_git_repo):
    local_clone_path = os.path.join(tmp_path, "repo_clone")
    os.mkdir(local_clone_path)
    repo = simple_git_repo
    repo.clone(local_clone_path)
    assert local_clone_needs_update(local_clone_path) is False

    needed_update = check_and_update(local_clone_path)
    assert needed_update is False


def test_clone_not_uptodate(tmp_path, simple_git_repo):
    local_clone_path = os.path.join(tmp_path, "repo_clone")
    os.mkdir(local_clone_path)
    repo = simple_git_repo
    local_clone = repo.clone(local_clone_path)

    # Reverting to the previous commit
    local_clone.git.reset("--hard", "HEAD~1")
    assert local_clone_needs_update(local_clone_path) is True

    needed_update = check_and_update(local_clone_path)
    assert needed_update is True

    # Now check that we are really up-to-date
    assert local_clone_needs_update(local_clone_path) is False


def test_git_update_needed(tmp_path, simple_git_repo, monkeypatch):
    transaction_args = []
    commits = []

    class CvmfsMock:
        def __init__(self, *args, **kwargs):
            transaction_args.append(*args)

        def __enter__(self):
            pass

        def __exit__(self, type, value, tb):
            if tb is None:
                commits.append(True)
            else:
                commits.append(False)

    # Change the environment
    monkeypatch.setattr("lbtaskrun.CvmfsTransaction", CvmfsMock)

    local_clone_path = os.path.join(tmp_path, "repo_clone")
    os.mkdir(local_clone_path)
    repo = simple_git_repo
    local_clone = repo.clone(local_clone_path)

    # Reverting to the previous commit
    local_clone.git.reset("--hard", "HEAD~1")
    assert local_clone_needs_update(local_clone_path) is True

    git_update("lhcb-condb.cern.ch", local_clone_path)

    # Now check that we are really up-to-date
    assert local_clone_needs_update(local_clone_path) is False
    assert commits[0] is True


def test_git_update_not_needed(tmp_path, simple_git_repo, monkeypatch):
    transaction_args = []
    commits = []

    class CvmfsMock:
        def __init__(self, *args, **kwargs):
            transaction_args.append(*args)

        def __enter__(self):
            pass

        def __exit__(self, type, value, tb):
            if tb is None:
                commits.append(True)
            else:
                commits.append(False)

    # Change the environment
    monkeypatch.setattr("lbtaskrun.CvmfsTransaction", CvmfsMock)

    local_clone_path = os.path.join(tmp_path, "repo_clone")
    os.mkdir(local_clone_path)
    simple_git_repo.clone(local_clone_path)

    try:
        git_update("lhcb-condb.cern.ch", local_clone_path)
    except Exception:
        pass  # This is normal, we should abort the transaction

    # Now check that we are really up-to-date
    assert local_clone_needs_update(local_clone_path) is False
    assert commits[0] is False


def test_check_and_update_bare(tmp_path, bare_git_repo, monkeypatch):
    repo = git.Repo(bare_git_repo)

    clone_path = clone_bare_repo(tmp_path, repo)
    clone_repo = git.Repo(clone_path)
    clone_repo.remotes.origin.fetch()
    assert repo.bare
    assert clone_repo.bare

    updated = check_and_update_bare(clone_path)
    assert updated is False

    update_bare_repo(tmp_path, bare_git_repo)
    updated = check_and_update_bare(clone_path)
    assert updated is True


def test_repack(tmp_path, simple_git_repo, monkeypatch):
    repo_dir = os.path.join(tmp_path, "test-git-repo")

    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(args)
            self.args = args
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    monkeypatch.setattr("git.cmd.Git.execute", SubprocessRunMock)

    repack(repo_dir)
    assert subprocess_run_args[0][0] == ["git", "repack", "--max-pack-size=100m"]


def test_gc(tmp_path, simple_git_repo, monkeypatch):
    repo_dir = os.path.join(tmp_path, "test-git-repo")

    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(args)
            self.args = args
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    monkeypatch.setattr("git.cmd.Git.execute", SubprocessRunMock)

    gc(repo_dir)
    assert subprocess_run_args[0][0] == ["git", "gc", "--auto"]


@pytest.mark.parametrize(
    ("message", "expected"),
    [
        ('---\ndatatypes: ["#Upgrade"]\n---', True),
        ("---\ndatatypes: [#Upgrade]\n---", False),
    ],
)
def test_gitcondb_tag_valid(message, expected):
    assert gitcondb_tag_valid(message) is expected
