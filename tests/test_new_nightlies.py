###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import platform
import subprocess

import pytest

from lbtaskrun.cvmfs import new_nightlies


@pytest.mark.skipif(platform.system() != "Linux", reason="Only supported on Linux")
def test_create_conda_env(tmp_path):
    env_yaml = "channels:\n" "  - conda-forge\n" "dependencies:\n" "  - cmake=3.18.4\n"
    try:
        new_nightlies.create_conda_env(tmp_path / "abcdef", env_yaml, "linux-64")
    except subprocess.CalledProcessError as e:
        assert (
            b"can't mount proc filesystem to /proc: operation not permitted" in e.stderr
            or b"fuse: device not found" in e.stderr
        )
        msg = "User namespaces are not available"
        raise pytest.skip(msg) from None
    out = subprocess.check_output([tmp_path / "abcdef/bin/cmake", "--version"])
    assert b"3.18.4" in out
