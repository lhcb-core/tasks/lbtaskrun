###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import json
import os
import platform
import shutil
import subprocess
from datetime import datetime, timedelta
from os.path import exists, join
from pathlib import Path

import pytest
import responses

from lbtaskrun.cvmfs import cleanup_nightlies, install_nightly, rsync_certificates
from lbtaskrun.cvmfs.nightlies import add_nightlies_link

test_date = datetime.now()
day_name = datetime.strftime(test_date, "%a")
DEFAULT_SLOTS = ["slot1", "slot2"]


def create_slot(path):
    # A week-old slot with the same day of the week as today
    os.mkdir(join(path, "0"))
    dayname = datetime.strftime(test_date, "%a")
    os.symlink("0", join(path, dayname))

    # The build from two days ago
    os.mkdir(join(path, "1"))
    dayname = datetime.strftime(test_date - timedelta(days=2), "%a")
    os.symlink("1", join(path, dayname))
    os.symlink("1", join(path, "Yesterday"))

    # The build from yesterday
    os.mkdir(join(path, "2"))
    dayname = datetime.strftime(test_date - timedelta(days=1), "%a")
    os.symlink("2", join(path, dayname))
    os.symlink("2", join(path, "Today"))
    os.symlink("2", join(path, "latest"))


@pytest.fixture()
def fake_couchdb(monkeypatch):
    monkeypatch.setattr(
        "lbtaskrun.cvmfs.nightlies.get_build_date",
        lambda slot_name, build_id: {
            "0": datetime.now() - timedelta(days=7),
            "1": datetime.now() - timedelta(days=2),
            "2": datetime.now() - timedelta(days=1),
        }[build_id].date(),
    )


@pytest.fixture()
def fake_nightly_slot(tmp_path, fake_couchdb):
    """Creates a fake nightlies slot directory"""
    create_slot(tmp_path)
    return tmp_path


@pytest.fixture()
def fake_nightlies(tmp_path, fake_couchdb):
    """Creates a fake nightlies directory"""
    for s in DEFAULT_SLOTS:
        slotpath = join(tmp_path, s)
        os.mkdir(slotpath)
        create_slot(slotpath)
    return tmp_path


def test_add_nightlies_links(fake_nightly_slot):
    build_id = 42
    os.mkdir(join(fake_nightly_slot, str(build_id)))
    add_nightlies_link(fake_nightly_slot, build_id, test_date)

    assert exists(join(fake_nightly_slot, "Today"))
    assert exists(join(fake_nightly_slot, day_name))
    assert exists(join(fake_nightly_slot, "latest"))
    for s in ["Today", day_name, "latest"]:
        assert os.readlink(join(fake_nightly_slot, s)) == str(build_id)


def test_installnightly(fake_nightlies, monkeypatch, fake_cvmfs_canary):
    # We need to mock subprocess.run to avoid a transaction being created
    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(*args)
            if args[0][0] == "lbn-install":
                # In this case we fake the creation of the new build directory
                # to make sure the links can be done
                dest = args[0][1]
                targetdir = dest.split("=")[1]
                os.makedirs(targetdir)
            self.args = args[0]
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    monkeypatch.setattr("subprocess.run", SubprocessRunMock)
    monkeypatch.setattr("lbtaskrun.cvmfs.get_build_date", lambda *x: datetime.now())
    monkeypatch.setattr("lbtaskrun.utils.logging_subprocess_run", SubprocessRunMock)

    class IsDirMock:
        def __init__(self, *args, **kwargs):
            print(f"isdir {args}")

    # Change the environment
    monkeypatch.setattr("os.path.isdir", IsDirMock)

    s = "slot1"
    slotpath = join(fake_nightlies, s)
    assert os.readlink(join(slotpath, "Today")) == "2"
    assert os.readlink(join(slotpath, "latest")) == "2"
    install_nightly(
        "lhcbdev.cern.ch",
        fake_nightlies,
        "slot1",
        "LHCb",
        42,
        ["x86_64-centos7-gcc9-opt"],
    )
    print(subprocess_run_args)
    assert exists(join(slotpath, "latest"))
    assert exists(join(slotpath, "Today"))
    assert exists(join(slotpath, day_name))
    assert os.readlink(join(slotpath, "Today")) == "42"
    assert os.readlink(join(slotpath, "latest")) == "42"
    assert os.readlink(join(slotpath, day_name)) == "42"
    assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
    assert subprocess_run_args[1] == ["cvmfs_server", "transaction", "lhcbdev.cern.ch"]


def test_installnightly_restrictlease(fake_nightlies, monkeypatch, fake_cvmfs_canary):
    # We need to mock subprocess.run to avoid a transaction being created
    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(*args)
            if args[0][0] == "lbn-install":
                # In this case we fake the creation of the new build directory
                # to make sure the links can be done
                dest = args[0][1]
                targetdir = dest.split("=")[1]
                os.makedirs(targetdir, exist_ok=True)
            self.args = args[0]
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    monkeypatch.setattr("subprocess.run", SubprocessRunMock)
    monkeypatch.setattr("lbtaskrun.cvmfs.get_build_date", lambda *x: datetime.now())
    monkeypatch.setattr("lbtaskrun.utils.logging_subprocess_run", SubprocessRunMock)
    s = "slot1"
    slotpath = join(fake_nightlies, s)
    assert os.readlink(join(slotpath, "Today")) == "2"
    assert os.readlink(join(slotpath, "latest")) == "2"
    install_nightly(
        "lhcbdev.cern.ch",
        fake_nightlies,
        "slot1",
        "LHCb",
        42,
        ["x86_64-centos7-gcc9-opt"],
        restrict_lease=True,
    )
    assert exists(join(slotpath, "latest"))
    assert exists(join(slotpath, "Today"))
    assert exists(join(slotpath, day_name))
    assert os.readlink(join(slotpath, "Today")) == "42"
    assert os.readlink(join(slotpath, "latest")) == "42"
    assert os.readlink(join(slotpath, day_name)) == "42"
    assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
    lockpath = subprocess_run_args[1][2]
    assert lockpath == str(slotpath)  # We should have locked the whole slot

    # Try to install another fake platform and make sure we had a more granular lock
    subprocess_run_args = []
    install_nightly(
        "lhcbdev.cern.ch",
        fake_nightlies,
        "slot1",
        "LHCb",
        42,
        ["x86_64-centos7-gcc9-dbg"],
        restrict_lease=True,
    )
    lockpath = subprocess_run_args[1][2]
    assert lockpath.endswith("/slot1/42")  # Now we should be more granular


def test_rsync_certificates(tmp_path, monkeypatch, cvmfs_mock, fake_cvmfs_canary):
    # We need to mock subprocess.run to avoid the command being run
    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(args)
            self.args = args
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    # Change the environment
    monkeypatch.setattr("subprocess.run", SubprocessRunMock)
    rsync_certificates("lhcbdev.cern.ch", str(tmp_path))
    cmd = subprocess_run_args[0][0]
    assert cmd[0] == "rsync"


@pytest.mark.skipif(not Path("/cvmfs/lhcb.cern.ch").is_dir(), reason="Requires CVMFS")
def test_update_platforms_list(tmp_path, cvmfs_mock):
    from lbtaskrun.cvmfs import update_platforms_list

    update_platforms_list("myrepo.example.com", tmp_path / "my-file.json")
    # Check the content is reasonable
    with open(tmp_path / "my-file.json") as fp:
        data = json.load(fp)
    assert "rows" in data
    assert len(data["rows"]) > 10


PIPELINE_DATA = """
[
  {
    "id": 15060099,
    "status": "success",
    "stage": "run",
    "name": "generate_projects_platforms",
    "ref": "master",
    "tag": false,
    "coverage": null,
    "allow_failure": false
  }
]
"""

PROJECT_PLATFORMS_DATA = """
{
    "DAVINCI": {
        "v50r6": [
            "x86_64-centos7-gcc9-opt",
            "x86_64-centos7-gcc8-do0",
            "x86_64-centos7-clang8-opt",
            "x86_64-centos7-gcc8-opt",
            "x86_64-centos7-gcc9-dbg",
            "x86_64+avx2+fma-centos7-gcc8-opt",
            "x86_64-centos7-clang8-dbg",
            "x86_64-centos7-gcc8-dbg"
        ],
        "v53r0": [
            "x86_64-centos7-gcc9-opt",
            "x86_64-centos7-gcc9-dbg",
            "x86_64-centos7-gcc9-do0",
            "x86_64-centos7-clang8-opt",
            "x86_64-centos7-clang8-dbg",
            "x86_64+avx2+fma-centos7-gcc9-opt"
        ]
    },
    "GAUDI": {
        "v35r4": [
            "x86_64-centos7-gcc9-opt",
            "x86_64-centos7-gcc9-dbg",
            "x86_64-centos7-gcc9-do0",
            "x86_64+avx2+fma-centos7-gcc9-opt"
        ],
        "v36r0": [
            "x86_64_v2-centos7-gcc10-opt",
            "x86_64_v2-centos7-gcc10-dbg"
        ]
    },
    "GAUSS": {
        "v41r0": [
            "i686-slc5-gcc43-opt",
            "x86_64-slc5-gcc43-opt"
        ],
        "v41r1": [
            "i686-slc5-gcc43-opt",
            "x86_64-slc5-gcc43-opt"
        ]
    }
}
"""


@pytest.fixture()
def mocked_pipeline():
    with responses.RequestsMock() as rsps:
        rsps.add(
            responses.GET,
            "https://gitlab.cern.ch/api/v4/projects/110853/pipelines/2805906/jobs",
            body=PIPELINE_DATA,
            status=200,
            content_type="application/json",
        )

        rsps.add(
            responses.GET,
            "https://gitlab.cern.ch/api/v4/projects/110853/jobs/15060099/"
            "artifacts/projects_platforms.json",
            body=PROJECT_PLATFORMS_DATA,
            status=200,
            content_type="application/json",
        )

        yield rsps


@pytest.mark.skipif(not Path("/cvmfs/lhcb.cern.ch").is_dir(), reason="Requires CVMFS")
def test_update_project_platforms_from_lhcbstacks(
    tmp_path, cvmfs_mock, mocked_pipeline
):
    from lbtaskrun.cvmfs import update_project_platforms_from_lhcbstacks

    # mocked_pipeline intercepting URL:
    # "https://gitlab.cern.ch/api/v4/projects/110853/pipelines/2805906/jobs"

    update_project_platforms_from_lhcbstacks(
        "myrepo.example.com", tmp_path / "my-file.json", 2805906
    )
    # Check the content is reasonable
    with open(tmp_path / "my-file.json") as fp:
        data = json.load(fp)
    assert "DAVINCI" in data


@pytest.mark.skipif(platform.system() != "Linux", reason="Only supported on Linux")
def test_create_lbenv(tmp_path, cvmfs_mock):
    from lbtaskrun.cvmfs import create_lbenv

    try:
        create_lbenv(
            "myrepo.example.com",
            tmp_path,
            flavours=["stable", "unstable"],
            platforms=["linux-64"],
        )
    except subprocess.CalledProcessError as e:
        assert (
            b"can't mount proc filesystem to /proc: operation not permitted" in e.stderr
        )
        msg = "User namespaces are not available"
        raise pytest.skip(msg) from None

    # Stable
    prefix = tmp_path / "2000" / "stable" / "linux-64"
    assert prefix.is_dir()
    result = subprocess.check_output(
        f"source {prefix}/bin/activate && lb-run --help", shell=True
    )
    assert b"Usage: lb-run" in result

    # Stable symlinks
    prefix = tmp_path / "stable" / "linux-64"
    assert not prefix.is_dir()
    prefix = tmp_path / "testing" / "linux-64"
    assert prefix.is_dir()
    result = subprocess.check_output(
        f"source {prefix}/bin/activate && lb-run --help", shell=True
    )
    assert b"Usage: lb-run" in result
    assert "/stable/" in str(prefix.readlink())

    # Unstable
    prefix = tmp_path / "2000" / "unstable" / "linux-64"
    assert prefix.is_dir()
    result = subprocess.check_output(
        f"source {prefix}/bin/activate && lb-run --help", shell=True
    )
    assert b"Usage: lb-run" in result

    # Unstable symlinks
    prefix = tmp_path / "unstable" / "linux-64"
    assert prefix.is_dir()
    result = subprocess.check_output(
        f"source {prefix}/bin/activate && lb-run --help", shell=True
    )
    assert b"Usage: lb-run" in result
    assert "/unstable/" in str(prefix.readlink())


@pytest.mark.skipif(platform.system() != "Linux", reason="Only supported on Linux")
def test_lhcbdirac_legacy(tmp_path, cvmfs_mock):
    from lbtaskrun.cvmfs import install_lhcbdirac, lhcbdirac_set_prod_link

    version = "v10.2.0a8"
    new_version = "v10.2.0a12"
    # Create a dummy dirac.cfg
    os.makedirs(tmp_path / "etc")
    (tmp_path / "etc" / "dirac.cfg").write_text("")
    # Install the original version
    install_lhcbdirac(version, "myrepo.example.com", tmp_path, alt_arch=False)
    assert exists(tmp_path / f"{version}-x86_64")
    assert exists(tmp_path / f"{version}-x86_64" / "bin" / "lhcb-proxy-init")
    assert not exists(tmp_path / f"{new_version}-x86_64" / "bin" / "lhcb-proxy-init")
    assert not exists(tmp_path / "prod")
    assert not exists(tmp_path / "prod.py3")
    # Create the initial prod symlink
    lhcbdirac_set_prod_link(version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())
    # Install the new version
    install_lhcbdirac(new_version, "myrepo.example.com", tmp_path, alt_arch=False)
    assert exists(tmp_path / f"{new_version}-x86_64")
    assert exists(tmp_path / f"{new_version}-x86_64" / "bin" / "lhcb-proxy-init")
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())
    # Update the prod symlink
    lhcbdirac_set_prod_link(new_version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert new_version in str(init_path.resolve())
        assert version not in str(init_path.resolve())
    # Roll-back the prod symlink
    lhcbdirac_set_prod_link(version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())


def tests_deploy_lhcbdirac_pilot(tmp_path, cvmfs_mock):
    from lbtaskrun.cvmfs import deploy_lhcbdirac_pilot

    deploy_lhcbdirac_pilot("myrepo.example.com", tmp_path)
    for name in [
        "checksums.sha512",
        "LHCbPilotCommands.py",
        "pilot.tar",
        "pilotCommands.py",
        "pilotTools.py",
    ]:
        assert (tmp_path / name).is_file()
    json.loads((tmp_path / "pilot.json").read_text())


@pytest.mark.skipif(platform.system() != "Linux", reason="Only supported on Linux")
def test_lhcbdirac_new(tmp_path, cvmfs_mock):
    from lbtaskrun.cvmfs import install_lhcbdirac, lhcbdirac_set_prod_link

    version = "v10.4.6"
    new_version = "v10.4.8"
    # Create a dummy dirac.cfg
    os.makedirs(tmp_path / "etc")
    (tmp_path / "etc" / "dirac.cfg").write_text("")
    # Install the original version
    install_lhcbdirac(version, "myrepo.example.com", tmp_path, alt_arch=False)
    assert exists(tmp_path / f"{version}" / "Linux-x86_64")
    assert exists(tmp_path / f"{version}" / "Linux-x86_64" / "bin" / "lhcb-proxy-init")
    assert not exists(
        tmp_path / f"{new_version}" / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
    )
    assert not exists(tmp_path / "prod")
    assert not exists(tmp_path / "prod.py3")
    # Create the initial prod symlink
    lhcbdirac_set_prod_link(version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())
    # Install the new version
    install_lhcbdirac(new_version, "myrepo.example.com", tmp_path, alt_arch=False)
    assert exists(tmp_path / f"{new_version}" / "Linux-x86_64")
    assert exists(
        tmp_path / f"{new_version}" / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
    )
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())
    # Update the prod symlink
    lhcbdirac_set_prod_link(new_version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert new_version in str(init_path.resolve())
        assert version not in str(init_path.resolve())
    # Roll-back the prod symlink
    lhcbdirac_set_prod_link(version, "myrepo.example.com", tmp_path)
    for link_name in ["prod", "prod.py3"]:
        init_path = tmp_path / link_name / "Linux-x86_64" / "bin" / "lhcb-proxy-init"
        assert init_path.exists()
        assert version in str(init_path.resolve())
        assert new_version not in str(init_path.resolve())


def test_nightly_cleanup(tmp_path, cvmfs_mock):
    # Make a fake nightlies dir
    nightlies_dir = tmp_path / "nightlies"
    shutil.copytree(
        Path(__file__).parent / "nightlies-install-example",
        nightlies_dir,
        symlinks=True,
    )

    flavours = ["nightly", "testing", "release"]
    slots = ["lhcb-gaudi-head", "lhcb-head", "lhcb-master", "lhcb-sim10"]

    # Make sure the expected paths exist
    for flavour in flavours:
        for slot in slots:
            slot_dir = nightlies_dir / flavour / slot
            assert slot_dir.is_dir()

            remaining_dirs = [x for x in (slot_dir).iterdir() if not x.is_symlink()]
            assert len(remaining_dirs) == 10
            for i in range(1, 11):
                target = slot_dir / str(i)
                assert target.exists()
                timestamp = (datetime.now() - timedelta(days=8 - i)).timestamp()
                os.utime(target, (timestamp, timestamp))

    # Do the cleanup
    cleanup_nightlies("example.invalid/nightlies", str(nightlies_dir))

    # Check the right cleanup has been done
    for flavour in flavours:
        for slot in slots:
            slot_dir = nightlies_dir / flavour / slot
            assert slot_dir.is_dir()

            remaining_dirs = [x for x in (slot_dir).iterdir() if not x.is_symlink()]
            assert len(remaining_dirs) == 9, (flavour, slot)
