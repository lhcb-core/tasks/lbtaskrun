###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
test delaing with the RPM installation on CVMFS
"""

from __future__ import annotations

import pytest
import responses

import lbtaskrun

BUILD_ID = 3845
BUILD_3845_DATA = """ {
"config": {
        "build_id": 3845,
        "slot": "lhcb-release",
        "packages": [],
        "description": "release build for stack Gaudi_v35r2 ",
        "cmake_cache": {
            "GAUDI_LEGACY_CMAKE_SUPPORT": true,
            "GAUDI_USE_INTELAMPLIFIER": true
        },
        "warning_exceptions": [],
        "build_tool": "cmake",
        "disabled": false,
        "platforms": [
            "x86_64-centos7-gcc9-opt",
            "x86_64-centos7-gcc9-dbg",
            "x86_64-centos7-gcc9-do0",
            "x86_64-centos7-clang8-opt",
            "x86_64-centos7-clang8-dbg",
            "x86_64+avx2+fma-centos7-gcc9-opt"
        ],
        "no_patch": true,
        "preconditions": [],
        "with_version_dir": true,
        "env": [],
        "error_exceptions": [],
        "projects": [
            {
                "name": "Gaudi",
                "checkout_opts": {
                    "export": true
                },
                "overrides": {},
                "disabled": false,
                "version": "v35r2",
                "env": [],
                "with_shared": false,
                "dependencies": [
                    "LCG"
                ],
                "checkout": "default"
            },
            {
                "name": "LCG",
                "checkout_opts": {
                    "export": true
                },
                "overrides": {},
                "disabled": true,
                "version": "97a",
                "env": [],
                "with_shared": false,
                "dependencies": [],
                "checkout": "lcg"
            }
        ]
      }
    }"""


@pytest.fixture()
def mocked_responses():
    with responses.RequestsMock() as rsps:
        rsps.add(
            responses.GET,
            f"https://lhcb-couchdb.cern.ch/nightlies-release/lhcb-release.{3845}",
            body=BUILD_3845_DATA,
            status=200,
            content_type="application/json",
        )

        yield rsps


def test_rpm_install(mocked_responses):
    """
    Testing that we read the correct data from the nightlies
    """
    rpm_names = lbtaskrun.rpm_utils.list_rpms_in_build(BUILD_ID)
    expected = [
        "GAUDI_v35r2",
        "GAUDI_v35r2_index",
        "GAUDI_v35r2_x86_64+avx2+fma_centos7_gcc9_opt",
        "GAUDI_v35r2_x86_64_centos7_clang8_dbg",
        "GAUDI_v35r2_x86_64_centos7_clang8_opt",
        "GAUDI_v35r2_x86_64_centos7_gcc9_dbg",
        "GAUDI_v35r2_x86_64_centos7_gcc9_do0",
        "GAUDI_v35r2_x86_64_centos7_gcc9_opt",
    ]
    assert rpm_names == expected
