###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import pytest

from lbtaskrun import rpm_utils


@pytest.mark.parametrize(
    ("fn", "rpm_project", "rpm_pkg_name", "rpm_pkg_version", "rpm_build_id"),
    [
        (
            "DBASE_WG_CharmWGProd_v0r0p1-1.0.0-1.noarch.rpm",
            "DBASE",
            "WG_CharmWGProd",
            "v0r0p1",
            "1",
        ),
        (
            "DBASE_WG_CharmConfig_v3r103-1.0.0-1.noarch.rpm",
            "DBASE",
            "WG_CharmConfig",
            "v3r103",
            "1",
        ),
        (
            "DBASE_AnalysisProductions_v0r1-1.0.0-2.noarch.rpm",
            "DBASE",
            "AnalysisProductions",
            "v0r1",
            "2",
        ),
        (
            "PARAM_TMVAWeights_v1r9-1.0.0-1.noarch.rpm",
            "PARAM",
            "TMVAWeights",
            "v1r9",
            "1",
        ),
    ],
)
def test_parse_rpm_fn_good(
    fn, rpm_project, rpm_pkg_name, rpm_pkg_version, rpm_build_id
):
    assert (
        rpm_project,
        rpm_pkg_name,
        rpm_pkg_version,
        rpm_build_id,
    ) == rpm_utils.parse_fn(fn)


@pytest.mark.parametrize(
    "fn",
    [
        "LCG_97apython3_LHCB_2LHCbGenerators_x86_64-centos7-clang10-opt-1.0.0-1.noarch.rpm",
        "REC_v31r1_x86_64_centos7_gcc9_do0-1.0.0-1.noarch.rpm",
        "DBASE_AppConfig_v3r398-2.0.0-1.noarch.rpm",
    ],
)
def test_parse_rpm_fn_bad(fn):
    with pytest.raises(ValueError):
        rpm_utils.parse_fn(fn)
